<%@page import="com.shah.pojo.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>CCBS</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />


<!-- Bootstrap core CSS     -->
<link href="css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="css/animate.min.css" rel="stylesheet" />

<!--  Light Bootstrap Table core CSS    -->
<link href="css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet" />


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="css/demo.css" rel="stylesheet" />


<!--     Fonts and icons     -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300'
	rel='stylesheet' type='text/css'>
<link href="css/pe-icon-7-stroke.css" rel="stylesheet" />


</head>
<body>
	<style>
input {
	margin-top: 10px;
}

.btn {
	background: blue;
	color: white;
	font: cursive;
}
</style>

	<div class="wrapper">
		<jsp:directive.include file="LeftSideBar.jsp" />

		<div class="main-panel">
			<jsp:directive.include file="TopBar.jsp" />
			<div class="content">
				<div class="container-fluid">
					<%
						String successMsg = (String) request.getAttribute("successMsg");
						if (successMsg != null) {
					%>
					<div class="row">
						<div class="col-md-12">

							<div class="alert alert-success">
								<button type="button" aria-hidden="true" class="close">x</button>
								<span><b> Success - </b> <%=successMsg%>"</span>
							</div>


						</div>

					</div>

					<%
						}
					%>

					<%
						String errorMsg = (String) request.getAttribute("errorMsg");
						if (errorMsg != null) {
					%>

					<div class="row">
						<div class="col-md-12">

							<div class="alert alert-danger">
								<button type="button" aria-hidden="true" class="close">x</button>
								<span><b> Error - </b> <%=errorMsg%></span>
							</div>

						</div>

					</div>

					<%
						}
					%>


					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h1 class="text-center">Update User Record</h1>
									<div class="content">
										<form class="form-horizontal" action="EditServletUser"
											method="post">
											<%
												User list = (User) request.getAttribute("userList");
											%>
											<div class="row">
												<div class="col-md-8 offset-6 text-center">
													<table>
														<tr>
															<td></td>
															<td><input type="hidden" name="id"
																value=<%=list.getId()%> /></td>
														</tr>
														<tr>
															<td>User Name:</td>
															<td><input type="text" name="username"
																class="form-control" required="required"
																value=<%=list.getUsername()%> /></td>
														</tr>
														<tr>
															<td>Password:</td>
															<td><input type="password" name="pswd"
																class="form-control" required="required"
																value=<%=list.getPassword()%> /></td>
														</tr>
														<tr>
															<td>First Name:</td>
															<td><input type="text" name="firstname"
																class="form-control" required="required"
																value=<%=list.getFirstname()%> /></td>
														</tr>
														<tr>
															<td>Last Name:</td>
															<td><input type="text" name="lastname"
																class="form-control" required="required"
																value=<%=list.getLastname()%> /></td>
														</tr>
														<tr>
															<td>Gender</td>
															<td><table>
																	<tr style="margin: 15px;">
																		<td><input type="radio" name="gender" value="m"
																			<%if (list.getGender().equals("m")) {%>
																			checked="checked" <%}%> style="margin-left: 15px;">Male</td>
																		<td><input type="radio" value="f" name="gender"
																			<%if (list.getGender().equals("f")) {%>
																			checked="checked" <%}%> style="margin-left: 15px;">Female</td>
																</table></td>
														</tr>
														<tr>
															<td>Email</td>
															<td><input type="email" name="email"
																class="form-control" required="required" 
																value=<%=list.getEmail()%> /></td>
														</tr>
														<tr>
															<td>Date_of_Birth:</td>
															<td><input type="date" name="dateofbirth"
																class="form-control" required="required" 
																 value=<%=list.getBirthdate()%> /></td>
														</tr>
														<tr>
															<td>Phone Number</td>
															<td><input type="number" name="phonenumber"
																class="form-control" required="required"
																value=<%=list.getPhoneNumber()%> /></td>
														</tr>
														<tr>
															<td colspan="2"><input type="submit"
																class="btn btn-success" value="Edit & Save " /></td>
														</tr>

													</table>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>


				<footer class="footer">
					<div class="container-fluid">
						<nav class="pull-left">
							<ul>
								<li><a href="#"> Home </a></li>
								<li><a href="#"> Company </a></li>
								<li><a href="#"> Portfolio </a></li>
								<li><a href="#"> Blog </a></li>
							</ul>
						</nav>
						<p class="copyright pull-right">
							&copy;
							<script>
								document.write(new Date().getFullYear())
							</script>
							<a href="http://www.creative-tim.com">Creative Tim</a>, made with
							love for a better web
						</p>
					</div>
				</footer>

			</div>
		</div>
	</div>
	
</body>

<!--   Core JS Files   -->
<script src="js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="js/demo.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						demo.initChartist();

						$
								.notify(
										{
											icon : 'pe-7s-gift',
											message : "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

										}, {
											type : 'info',
											timer : 4000
										});

					});
</script>


</html>
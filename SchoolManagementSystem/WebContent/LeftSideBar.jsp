<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<div class="sidebar" data-color="purple"
			data-image="assets/img/sidebar-5.jpg">

			<!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

			<div class="sidebar-wrapper">
				<div class="logo">
					<a  class="simple-text">
						FYP  </a>
				</div>

				<ul class="nav">
					<li class="active"> <i
							class="pe-7s-graph"></i>
							<p>Admin</p>
					</a></li>
						<li><a href="EntryStudentServlet"> <i class="pe-7s-news-paper"></i>
							<p>Rgister Student</p>
					</a></li>
					<li><a href="ViewServlet"> <i class="pe-7s-user"></i>
							<p>Student Record</p>
					</a></li>
					<li><a href="AddTeacherServlet"> <i class="pe-7s-note2"></i>
							<p> Register Teacher</p>
					</a></li>
					<li><a href="TeacherViewServlet"> <i class="pe-7s-map-marker"></i>
							<p>Teacher Record</p>
					</a></li>
				
					<li><a href="Subjects.jsp"> <i class="pe-7s-science"></i>
							<p>Subject Entry</p>
					</a></li>
					<li><a href="SubjectViewServlet"> <i class="pe-7s-science"></i>
							<p>Subject List</p>
					</a></li>
					<li><a href="AddClassSaveServlet"> <i class="pe-7s-bell"></i>
							<p>Classes Entry</p>
					</a></li>
										<li><a href="StudentAddressViewServlet"> <i class="pe-7s-bell"></i>
							<p>Student Address Entry</p>
					</a></li>

					<li class="active-pro"><a href=""> <i
							class="pe-7s-rocket"></i>
							<p>All Classes</p>
					</a></li>
				</ul>
			</div>
		</div>

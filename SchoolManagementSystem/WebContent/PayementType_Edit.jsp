<%@page import="com.shah.pojo.PayementType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>CCBS</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />


<!-- Bootstrap core CSS     -->
<link href="css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="css/animate.min.css" rel="stylesheet" />

<!--  Light Bootstrap Table core CSS    -->
<link href="css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet" />


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="css/demo.css" rel="stylesheet" />


<!--     Fonts and icons     -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300'
	rel='stylesheet' type='text/css'>
<link href="css/pe-icon-7-stroke.css" rel="stylesheet" />


</head>
<body>
	<style>
input {
	margin-top: 10px;
}

textarea {
	margin-top: 10px;
	margin-bottom: 10px;
	color: blue;
}

.btn {
	background: blue;
	color: white;
	font: cursive;
}
</style>

	<div class="wrapper">
		<jsp:directive.include file="LeftSideBar.jsp" />

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Dashboard</a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"> <i class="fa fa-dashboard"></i>
									<p class="hidden-lg hidden-md">Dashboard</p>
							</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"> <i class="fa fa-globe"></i> <b
									class="caret hidden-lg hidden-md"></b>
									<p class="hidden-lg hidden-md">
										5 Notifications <b class="caret"></b>
									</p>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#">Notification 1</a></li>
									<li><a href="#">Notification 2</a></li>
									<li><a href="#">Notification 3</a></li>
									<li><a href="#">Notification 4</a></li>
									<li><a href="#">Another notification</a></li>
								</ul></li>
							<li><a href=""> <i class="fa fa-search"></i>
									<p class="hidden-lg hidden-md">Search</p>
							</a></li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<li><a href="">
									<p>Account</p>
							</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">
									<p>
										Dropdown <b class="caret"></b>
									</p>

							</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul></li>
							<li><a href="#">
									<p>Log out</p>
							</a></li>
							<li class="separator hidden-lg"></li>
						</ul>
					</div>
				</div>
			</nav>


			<div class="content">
				<div class="container-fluid">


					<div class="content">
						<div class="container-fluid">
							<%
								String successMsg = (String) request.getAttribute("successMsg");
								if (successMsg != null) {
							%>
							<div class="row">
								<div class="col-md-12">

									<div class="alert alert-success">
										<button type="button" aria-hidden="true" class="close">x</button>
										<span><b> Success - </b> <%=successMsg%>"</span>
									</div>


								</div>

							</div>

							<%
								}
							%>

							<%
								String errorMsg = (String) request.getAttribute("errorMsg");
								if (errorMsg != null) {
							%>

							<div class="row">
								<div class="col-md-12">

									<div class="alert alert-danger">
										<button type="button" aria-hidden="true" class="close">x</button>
										<span><b> Error - </b> <%=errorMsg%></span>
									</div>

								</div>

							</div>

							<%
								}
							%>


							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="header">
											<h1 class="title">update PayementType</h1>
										</div>
										<div class="content">
											<form class="form-horizontal"
												action="PayementTypeEditServlet1" method="post">
												<%
													PayementType payementTypeList = (PayementType) request.getAttribute("payementTypeList");
												%>
												<div class="row">
													<div class="col-md-8 offset-6 text-center">
														<table class="">
															<tr>
																<td></td>
																<td><input type="hidden" name="id"
																	value=<%=payementTypeList.getId()%> /></td>
															</tr>
															<tr>
																<td>Name:</td>
																<td><input type="text" name="name"
																	class="form-control" required="required"
																	value=<%=payementTypeList.getName()%> /></td>
															</tr>
															<tr>
																<td colspan="2"><input type="submit"
																	value="Edit & Save " class="btn btn-success" /></td>
															</tr>


														</table>
													</div>
												</div>

											</form>

										</div>
									</div>

								</div>
							</div>


							<footer class="footer">
								<div class="container-fluid">
									<nav class="pull-left">
										<ul>
											<li><a href="#"> Home </a></li>
											<li><a href="#"> Company </a></li>
											<li><a href="#"> Portfolio </a></li>
											<li><a href="#"> Blog </a></li>
										</ul>
									</nav>
									<p class="copyright pull-right">
										&copy;
										<script>
											document.write(new Date()
													.getFullYear())
										</script>
										<a href="http://www.creative-tim.com">Creative Tim</a>, made
										with love for a better web
									</p>
								</div>
							</footer>

						</div>
					</div>
				</div>
</body>

<!--   Core JS Files   -->
<script src="js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="js/demo.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						demo.initChartist();

						$
								.notify(
										{
											icon : 'pe-7s-gift',
											message : "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

										}, {
											type : 'info',
											timer : 4000
										});

					});
</script>



</html>
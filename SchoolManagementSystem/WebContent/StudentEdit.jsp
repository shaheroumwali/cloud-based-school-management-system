<%@page import="com.shah.pojo.Addresses"%>
<%@page import="com.shah.pojo.StudentAddress"%>
<%@page import="com.shah.pojo.Student"%>
<%@page import="com.shah.pojo.Class"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.shah.pojo.School"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>CCBS</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />


<!-- Bootstrap core CSS     -->
<link href="css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="css/animate.min.css" rel="stylesheet" />

<!--  Light Bootstrap Table core CSS    -->
<link href="css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet" />


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="css/demo.css" rel="stylesheet" />


<!--     Fonts and icons     -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300'
	rel='stylesheet' type='text/css'>
<link href="css/pe-icon-7-stroke.css" rel="stylesheet" />


</head>
<body>
	<style>
input {
	margin-top: 10px;
}

textarea {
	margin-top: 10px;
	margin-bottom: 10px;
	color: blue;
}

.btn {
	background: blue;
	color: white;
	font: cursive;
}

input[gender] {
	margin: 10px;
}
</style>

	<div class="wrapper">
		<jsp:directive.include file="LeftSideBar.jsp" />

		<div class="main-panel">
			<jsp:directive.include file="TopBar.jsp" />
			<div class="content">
				<div class="container-fluid">


					<div class="content">
						<div class="container-fluid">
							<%
								String successMsg = (String) request.getAttribute("successMsg");
								if (successMsg != null) {
							%>
							<div class="row">
								<div class="col-md-12">

									<div class="alert alert-success">
										<button type="button" aria-hidden="true" class="close">x</button>
										<span><b> Success - </b> <%=successMsg%>"</span>
									</div>


								</div>

							</div>

							<%
								}
							%>

							<%
								String errorMsg = (String) request.getAttribute("errorMsg");
								if (errorMsg != null) {
							%>

							<div class="row">
								<div class="col-md-12">

									<div class="alert alert-danger">
										<button type="button" aria-hidden="true" class="close">x</button>
										<span><b> Error - </b> <%=errorMsg%></span>
									</div>

								</div>

							</div>

							<%
								}
							%>


							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="header">

											<h4 class="title">Add New Student</h4>
											<p class="category">Add a new student form in a school
												and all its relevant details</p>

											<div class="content">
												<form class="form-horizontal" action="EditServlet"
													method="post">
													<%
														Student std = (Student) request.getAttribute("studentObject");
													%>
													<div class="row">
														<div class="col-md-8 offset-6 text-center">
															<table>
																<tr>
																	<td>RollNo:</td>
																	<td><input type="number" name="rollno"
																		value="<%=std.getRollno()%>" class="form-control" />
																		<input type="hidden" name="id"
																		value="<%=std.getId()%>" /></td>
																</tr>
																<tr>
																	<td>Gender</td>
																	<td><table>
																			<tr style="margin: 15px;">
																				<td><input type="radio" name="gender" value="m"
																					<%if (std.getgender().equals("m")) {%>
																					checked="checked" <%}%> style="margin-left: 15px;">Male</td>
																				<td><input type="radio" value="f" name="gender"
																					<%if (std.getgender().equals("f")) {%>
																					checked="checked" <%}%> style="margin-left: 15px;">Female</td>
																			</tr>
																		</table></td>
																</tr>
																<tr>
																	<td>First Name:</td>
																	<td><input type="text" class="form-control" 
																		value="<%=std.getfirstname()%>" name="firstname" /></td>
																</tr>
																<tr>
																	<td>Last Name:</td>
																	<td><input type="text" class="form-control"
																		value="<%=std.getlastname()%>" name="lastname" /></td>
																</tr>
																<tr>
																	<td>Date_of_Birth:</td>
																	<td><input type="date" class="form-control"
																		value="<%=std.getDOB()%>" name="dateofbirth" /></td>
																</tr>
																<tr>
																	<td>Tuition_Fee</td>
																	<td><input type="number" class="form-control"
																		value="<%=std.getTuition_fee()%>" name="tuitionfee"></td>
																</tr>

																<tr>
																	<td>Other Students Details</td>
																	<td><textarea name="otherStudentDetail"
																			class="form-control" rows="4" cols="80"><%=std.getOther_student_detail()%></textarea></td>
																</tr>
																<tr>
																	<td>Select School <%=std.getSchool_id()%></td>
																	<td><select name="school_id" class="form-control">
																			<%
																				ArrayList<School> schoolList = (ArrayList<School>) request.getAttribute("schoolList");
																				for (int i = 0; i < schoolList.size(); i++) {
																			%>

																			<option value="<%=schoolList.get(i).getId()%>"
																				<%if (std.getSchool_id() == schoolList.get(i).getId()) {%>
																				selected="selected" <%}%>><%=schoolList.get(i).getName()%>
																			</option>


																			<%
																				}
																			%>
																	</select></td>
																</tr>
																<tr>

																	<td colspan="8">Class Details</td>
																</tr>

																<tr>
																	<td>Class Date Address From:</td>
																	<td><input type="date" class="form-control"
																		value=<%=std.getStudentClasses().getDateAddressFrom()%>
																		name="ClassDateFrom"  /></td>
																</tr>

																<tr>
																	<td>Class Date Address To:</td>
																	<td><input type="date" class="form-control"
																		value=<%=std.getStudentClasses().getDateAddressTo()%>
																		name="ClassDateTo" /></td>
																</tr>
																<tr>
																	<td>Select Class</td>
																	<td><select name="classes_id" class="form-control">

																			<%
																				ArrayList<com.shah.pojo.Class> ClassList = (ArrayList<com.shah.pojo.Class>) request
																						.getAttribute("ClassList");
																				for (int i = 0; i < ClassList.size(); i++) {
																			%>

																			<option value="<%=ClassList.get(i).getId()%>"
																				<%if (std.getStudentClasses().getClass_id() == ClassList.get(i).getId()) {%>
																				selected="selected" <%}%>>
																				<%=ClassList.get(i).getName()%>
																			</option>


																			<%
																				}
																			%>


																	</select></td>
																</tr>

																<tr>
																	<td colspan="8">Student Address</td>
																</tr>
																<tr>
																	<td>Student Date Address From:</td>
																	<td><input type="date" class="form-control"
																		value=<%=std.getStudentAddress().getDateAddressFrom()%>
																		name="studentAddressFrom" /></td>
																</tr>
																<tr>
																	<td>Student Date Address To:</td>
																	<td><input type="date" class="form-control"
																		value=<%=std.getStudentAddress().getDateAddressTo()%>
																		name="studentAddressTo" /></td>
																</tr>
																<tr>
																	<td>Address Details</td>
																	<td><textarea name="addressDetail"
																			class="form-control" rows="4" cols="80"><%=std.getStudentAddress().getAddressDetail()%></textarea></td>
																</tr>
																<!-- 					<tr>
																<td colspan="8">Student Parent</td>
															</tr>
															<tr>
																<td>First Name:</td>
																<td><input type="text" name="firstname" 
																class="form-control" /></td>
															</tr>
															<tr>
																<td>Last Name:</td>
																<td><input type="text" name="lastname"  
																class="form-control" /></td>
															</tr>
 -->


																<tr>
																	<td colspan="2"><input type="submit"
																		value="Edit & Save " class="btn btn-success" /></td>
																</tr>
															</table>
														</div>
													</div>

												</form>

											</div>
										</div>

									</div>
								</div>


								<footer class="footer">
									<div class="container-fluid">
										<nav class="pull-left">
											<ul>
												<li><a href="#"> Home </a></li>
												<li><a href="#"> Company </a></li>
												<li><a href="#"> Portfolio </a></li>
												<li><a href="#"> Blog </a></li>
											</ul>
										</nav>
										<p class="copyright pull-right">
											&copy;
											<script>
												document.write(new Date()
														.getFullYear())
											</script>
											<a href="http://www.creative-tim.com">Creative Tim</a>, made
											with love for a better web
										</p>
									</div>
								</footer>

							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
</body>

<!--   Core JS Files   -->
<script src="js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="js/demo.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						demo.initChartist();

						$
								.notify(
										{
											icon : 'pe-7s-gift',
											message : "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

										}, {
											type : 'info',
											timer : 4000
										});

					});
</script>


</html>
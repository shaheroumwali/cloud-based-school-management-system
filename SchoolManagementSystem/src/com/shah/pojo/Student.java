package com.shah.pojo;

public class Student {
	private int id,rollno,created_at,modified_at,created_by,modified_by,school_id,StudentClasses_id,StudentAddress_id,Addresses_id;
	private String firstname,dateofbirth,lastname,gender,other_student_detail;
	private double tuition_fee;
	private School school;
	private StudentClasses StudentClasses;
	private StudentAddress StudentAddress;
	private Addresses  Addresses;
	public int getStudentClasses_id() {
		return StudentClasses_id;
	}
	public void setStudentClasses_id(int studentClasses_id) {
		StudentClasses_id = studentClasses_id;
	}
	public int getStudentAddress_id() {
		return StudentAddress_id;
	}
	public void setStudentAddress_id(int studentAddress_id) {
		StudentAddress_id = studentAddress_id;
	}
	public int getAddresses_id() {
		return Addresses_id;
	}
	public void setAddresses_id(int addresses_id) {
		Addresses_id = addresses_id;
	}
	public int getSchool_id() {
		return school_id;
	}
	public void setSchool_id(int school_id) {
		this.school_id = school_id;
	}
	
    public StudentAddress getStudentAddress() {
		return StudentAddress;
	}
	public void setStudentAddress(StudentAddress studentAddress) {
		StudentAddress = studentAddress;
	}
	public Addresses getAddresses() {
		return Addresses;
	}
	public void setAddresses(Addresses addresses) {
		Addresses = addresses;
	}

	public StudentClasses getStudentClasses() {
		return StudentClasses;
	}
	public void setStudentClasses(StudentClasses studentClasses) {
		StudentClasses = studentClasses;
	}
	public School getSchool() {
		return school;
	}
	public void setSchool(School school) {
		this.school = school;
	}
	public int getId() {
		return id;
	}
	public void  setId(int id) {
		this.id=id;
	}
	public int getRollno() {
		return rollno;
	}
	public void setRollno(int rollno) {
		this.rollno=rollno;
	}
	public String getDOB() {
		return dateofbirth;
	} 
	public void setDOB(String dateofbirth) {
		this.dateofbirth=dateofbirth;
	}
	public  String getfirstname() {
		return firstname;
	} 
	public void setfirstname(String firstname) {
	   this.firstname=firstname;
	}
	public String getlastname() {
		return lastname;
	} 
	public void setlastname(String lastname) {
	this.lastname=lastname;	
	}
	public String getgender() {
		return gender;
	}
	public void setgender(String gender) {
	 this.gender=gender;

	}
	public String getOther_student_detail() {
		return other_student_detail;
	}
	public void setOther_student_detail(String other_student_detail) {
		this.other_student_detail = other_student_detail;
	}
	
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public int getModified_by() {
		return modified_by;
	}
	public void setModified_by(int modified_by) {
		this.modified_by = modified_by;
	}
	public int getCreated_at() {
		return created_at;
	}
	public void setCreated_at(int created_at) {
		this.created_at = created_at;
	}
	public int getModified_at() {
		return modified_at;
	}
	public void setModified_at(int modified_at) {
		this.modified_at = modified_at;
	}
	public double getTuition_fee() {
		return tuition_fee;
	}
	public void setTuition_fee(double tuition_fee) {
		this.tuition_fee = tuition_fee;
	}
	
}
	


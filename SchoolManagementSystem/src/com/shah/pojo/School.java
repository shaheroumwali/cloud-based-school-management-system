package com.shah.pojo;

public class School {

	private int id,created_by,modified_by,addressesId;
	private String name,PrincipalName,other_school_details,created_at,modified_at;
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public int getModified_by() {
		return modified_by;
	}
	public void setModified_by(int modified_by) {
		this.modified_by = modified_by;
	}
	public int getAddressesId() {
		return addressesId;
	}
	public void setAddressesId(int addressesId) {
		this.addressesId = addressesId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrincipalName() {
		return PrincipalName;
	}
	public void setPrincipalName(String principalName) {
		PrincipalName = principalName;
	}
	public String getOther_school_details() {
		return other_school_details;
	}
	public void setOther_school_details(String other_school_details) {
		this.other_school_details = other_school_details;
	}
    public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getModified_at() {
		return modified_at;
	}
	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}
	
}

package com.shah.pojo;

public class PayementItem {
	private int id,createdBy,modifiedBy,payement_id;
	public int getPayement_id() {
		return payement_id;
	}
	public void setPayement_id(int payement_id) {
		this.payement_id = payement_id;
	}
	private String description,createdAt,modifiedAt,status;
	private double amount;
	private Payements  payement;
	
	
	public Payements getPayement() {
		return payement;
	}
	public void setPayement(Payements payement) {
		this.payement = payement;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public int getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}

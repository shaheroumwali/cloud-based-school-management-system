package com.shah.pojo;

public class ParentAddress {

private int Id,createdBy,modifiedBy,addresses_id,parent_id;


private String dateAddressFrom,dateAddressTo,createdAt,modifiedAt,status;
private Addresses addresses;
private Parent parent;

public int getAddresses_id() {
	return addresses_id;
}
public void setAddresses_id(int addresses_id) {
	this.addresses_id = addresses_id;
}
public int getParent_id() {
	return parent_id;
}
public void setParent_id(int parent_id) {
	this.parent_id = parent_id;
}
public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public Parent getParent() {
	return parent;
}
public void setParent(Parent parent) {
	this.parent = parent;
}
public Addresses getAddresses() {
	return addresses;
}
public void setAddresses(Addresses address) {
	addresses = address;
}

public int getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(int createdBy) {
	this.createdBy = createdBy;
}
public int getModifiedBy() {
	return modifiedBy;
}
public void setModifiedBy(int modifiedBy) {
	this.modifiedBy = modifiedBy;
}
public String getDateAddressFrom() {
	return dateAddressFrom;
}
public void setDateAddress_from(String dateAddressFrom) {
	this.dateAddressFrom = dateAddressFrom;
}
public String getDateAddressTo() {
	return dateAddressTo;
}
public void setDateddressT(String dateAddressTo) {
	this.dateAddressTo = dateAddressTo;
}
public String getCreatedAt() {
	return createdAt;
}
public void setCreatedAt(String createdAt) {
	this.createdAt = createdAt;
}
public String getModifiedAt() {
	return modifiedAt;
}
public void setModifiedAt(String modifiedAt) {
	this.modifiedAt = modifiedAt;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
}

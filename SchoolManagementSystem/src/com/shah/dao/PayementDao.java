package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.PayementType;
import com.shah.pojo.Payements;
import com.shah.pojo.Student;
public class PayementDao {
	
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(Payements e) {
		int status=0;
		try {
			Connection con = PayementDao.getConnection();
		
			PreparedStatement ps = con.prepareStatement("insert into payements("
					+ "amount,"
					+ "payement_date,"
					+ "p_type_id,"
					+ "created_at"
					+ ",modified_at,"
					+ "created_by,"
					+ "modified_by,"
					+ "payement_type_id,"
					+ "students_id) values (?,?,?,?,?,?,?,?,?)");
	 		ps.setDouble(1,e.getAmount());
			ps.setString(2, e.getPayDate());
			ps.setInt(3,e.getPayTypeId());
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(4, cr_dt_time);
			ps.setTimestamp(5, cr_dt_time);
			ps.setInt(6, e.getCreatedBy());
			ps.setInt(7, e.getModifiedBy());
			ps.setInt(8,e.getPayementType().getId());
			ps.setInt(9,e.getStudent().getId());

			status = ps.executeUpdate();
		}
		catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
		
	}
	public static int update(Payements e){  
	    int status=0;  
	    try{  
	        Connection con=PayementDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update payements set amount=?,payement_date=?,p_type_id=?,payement_type_id=?,students_id=? where id=?");  
	        ps.setDouble(1,e.getAmount());
	        ps.setString(2,e.getPayDate());
			ps.setInt(3,e.getPayTypeId());
			ps.setInt(4,e.getPayementType_id());
			ps.setInt(5, e.getStudent_id());
			ps.setInt(6, e.getId());
			status = ps.executeUpdate();
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return status;  
	}
	public static int delete(int id){  
	    int status=0;  
	    try{  
	        Connection con=PayementDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("delete from payements where id=?");  
	        ps.setInt(1,id);  
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return status;  
	}
	public static Payements getPayementById(int id){  
	    Payements e=new Payements();      
	    try{  
	        Connection con=PayementDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from payements where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        if(rs.next()){  
	        	e.setId(rs.getInt(1));
	        	e.setAmount(rs.getDouble(2));
	        	  e.setPayDate(rs.getString(3));
	        	  e.setPayTypeId(rs.getInt(4));
	        	   e.setPayementType_id(rs.getInt(9));
	        	   e.setStudent_id(rs.getInt(10));
		                 }  
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return e;  
	}
	public static List<Payements> getAllPayements(){  
	    List<Payements> list=new ArrayList<Payements>();  
	      
	    try{  
	        Connection con=PayementDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from payements");  
	        ResultSet rs=ps.executeQuery();  
	        while(rs.next()){  
	            Payements e=new Payements();  
	      	  e.setId(rs.getInt(1));
	      	  e.setAmount(rs.getDouble(2));
      	      e.setPayDate(rs.getString(3));
      	      e.setPayTypeId(rs.getInt(4));
      	      list.add(e);  
	        }  
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return list;  
	}
	public static List<Payements> getAllPayementWithPayementTypeAndStName(){  
	    List<Payements> list=new ArrayList<Payements>();  
	      
	    try{  
	        Connection con=PayementDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("SELECT p.*,pt.name,st.first_name,st.last_name FROM school_management_system.payements p LEFT OUTER JOIN payement_type pt ON pt.id=p.payement_type_id LEFT OUTER JOIN students st ON p.students_id=st.id");  
	        ResultSet rs=ps.executeQuery();  
	        while(rs.next()){  
	            Payements e=new Payements();  
	      	  e.setId(rs.getInt(1));
	      	  e.setAmount(rs.getDouble(2));
      	      e.setPayDate(rs.getString(3));
      	      e.setPayTypeId(rs.getInt(4));
      	      
      	      PayementType pT = new PayementType();
      	     pT.setId(rs.getInt(9));
      	     pT.setName(rs.getString(12));
      	     e.setPayementType(pT);
      	     
         	Student s = new Student();
      	
      	     s.setId(rs.getInt(10));
      	     s.setfirstname(rs.getString(13));
      	     s.setlastname(rs.getString(14));
      	     e.setStudent(s);
      	   list.add(e);  
	        }  
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return list;  
	}
}
 
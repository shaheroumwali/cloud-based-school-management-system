package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Addresses;
import com.shah.pojo.School;
import com.shah.pojo.SchoolAddress;

public class SchoolAddressDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}



public static int save(SchoolAddress e) {
	int status=0;
	try {
		Connection con = SchoolAddressDao.getConnection();
	
		PreparedStatement ps = con.prepareStatement("insert into schools_addresses("
				+"schools_id,"
				+ "addresses_id,"
				+ "created_at,"
				+ "modified_at,"
				+ "created_by,"	
				+ "modified_by"
				+ ") values (?,?,?,?,?,?)");
		ps.setInt(1,e.getSchool().getId());
		ps.setInt(2,e.getAddresses().getId());
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(3, cr_dt_time);
		ps.setTimestamp(4, cr_dt_time);
		ps.setInt(5, e.getSchool().getId());
		ps.setInt(6, e.getAddresses().getId());
	
		
//		System.out.println(e.getParent().getId());
		
		status = ps.executeUpdate();
	}
	catch (Exception e2) {
		System.out.println(e2 + " error in database save method");
	}
	return status;
}
public static SchoolAddress getSchoolAddressById(int id){  
	SchoolAddress e=new SchoolAddress();      
    try{  
        Connection con=SchoolAddressDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from schools_addresses where id=?");  
        ps.setInt(1,id);  
        ResultSet rs=ps.executeQuery();  
        if(rs.next()){  
        	e.setId(rs.getInt(1));
        	e.setSchool_id(rs.getInt(2));
        	e.setAddresses_id(rs.getInt(3));
        	
	                 }  
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return e;  
}
public static int update(SchoolAddress e){  
    int status=0;  
    try{  
        Connection con=SchoolAddressDao.getConnection();  
        PreparedStatement ps=con.prepareStatement(  
        "update schools_addresses set schools_id=?,addresses_id=? where id=?");  
    	ps.setInt(1,e.getSchool_id());
		ps.setInt(2, e.getAddresses_id());
        ps.setInt(3,e.getId());
		status = ps.executeUpdate();
        con.close();  
    }catch(Exception ex)
    {ex.printStackTrace();}  
    return status;  
}

public static int delete(int id){  
    int status=0;
    try{  
        Connection con=SchoolAddressDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("delete from schools_addresses where id=?");  
        ps.setInt(1,id);  
        status=ps.executeUpdate();  
          
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return status;  
}
public static List<SchoolAddress> getAllSchoolAddressWithSchoolNameandAddrs () {
	List<SchoolAddress> list = new ArrayList<SchoolAddress>();

	try {
		Connection con = SchoolAddressDao.getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT schAddr.*,sch.name,sch.school_principal,addr.address_details FROM school_management_system.schools_addresses schAddr  LEFT OUTER JOIN schools sch ON sch.id=schAddr.schools_id LEFT OUTER JOIN  addresses addr ON schAddr.addresses_id=addr.id;");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			SchoolAddress e = new SchoolAddress();
			e.setId(rs.getInt(1));
			
			
			School s = new School();
			s.setId(rs.getInt(2));
			s.setName(rs.getString(9));
			s.setPrincipalName(rs.getString(10));
			e.setSchool(s);
			
			Addresses addr = new Addresses();
			addr.setId(rs.getInt(3));
			addr.setAddressDetails(rs.getString(11));
			e.setAddresses(addr);
           
            		list.add(e);
		}
		con.close();
	} catch (Exception e) {
		
		System.out.println("Error In getAllSchoolAddrWithSchoolNameANDAddresses ");
	}

	return list;
}


}




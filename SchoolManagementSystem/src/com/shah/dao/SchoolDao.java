package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shah.pojo.School;

public class SchoolDao {
public static Connection getConnection() {
		
		Connection con=null;
	    try {
	    	Class.forName("com.mysql.jdbc.Driver");  
        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root" , "shaheroum1997");
	    
        
	    }
	    catch(Exception ex ) {
	    	System.out.println("database error" + ex);
	    }
		return con;
	}
public static int save(School e) {
	int status=0;
	try {
		Connection con = SchoolDao.getConnection();
	
		PreparedStatement ps = con.prepareStatement("insert into schools("
				+ "name,"
				+ "school_principal,"
				+ "other_school_details,"
				+ "created_at"
				+ ",modified_at,"
				+ "created_by,"
				+ "modified_by) values (?,?,?,?,?,?,?)");
 		ps.setString(1, e.getName());
		ps.setString(2, e.getPrincipalName());
		ps.setString( 3,e.getOther_school_details());
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(4, cr_dt_time);
		ps.setTimestamp(5, cr_dt_time);
		ps.setInt(6, e.getCreated_by());
		ps.setInt(7, e.getModified_by());
//		ps.setInt(8, 2);
		status = ps.executeUpdate();
	}
	catch (Exception e2) {
		System.out.println(e2 + " error in database save method");
	}
	return status;
	
}
public static int update(School e){  
    int status=0;  
    try{  
        Connection con=StudentDao.getConnection();  
        PreparedStatement ps=con.prepareStatement(  
        "update schools set name=?,school_principal=?,other_school_details=? where id=?");  
        ps.setString(1, e.getName());
		ps.setString(2, e.getPrincipalName());
		ps.setString( 3,e.getOther_school_details());
		ps.setInt(4, e.getId());
        status=ps.executeUpdate();  
          
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return status;  
}
public static int delete(int id){  
    int status=0;  
    try{  
        Connection con=SchoolDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("delete from schools where id=?");  
        ps.setInt(1,id);  
        status=ps.executeUpdate();  
          
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return status;  
}
public static School getSchoolById(int id){  
    School e=new School();  
      
    try{  
        Connection con=SchoolDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from schools where id=?");  
        ps.setInt(1,id);  
        ResultSet rs=ps.executeQuery();  
        if(rs.next()){  
        	  e.setId(rs.getInt(1));
        	  e.setName(rs.getString(2));
        	  e.setPrincipalName(rs.getString(3));
        	  e.setOther_school_details(rs.getString(4));
        	   
	                 }  
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return e;  
}

	public static List<School> getAllSchools() {
		List<School> list = new ArrayList<School>();

		try {
			Connection con = SchoolDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from schools");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				School e = new School();
				e.setId(rs.getInt(1));
				e.setName(rs.getString(2));
				e.setPrincipalName(rs.getString(3));
				e.setOther_school_details(rs.getString(4));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	

	public static School getSchoolByUserId(int userID) {
		School school = new School();

		try {
			Connection con = SchoolDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select s.* from schools s LEFT OUTER JOIN users_schools us ON us.schools_id=s.id where us.users_id=? LIMIT 1");
			ps.setInt(1, userID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				school.setId(rs.getInt(1));
				school.setName(rs.getString(2));
				school.setPrincipalName(rs.getString(3));
				school.setOther_school_details(rs.getString(4));
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return school;
	}
}

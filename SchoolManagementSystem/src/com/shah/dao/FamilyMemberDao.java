package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Family;
import com.shah.pojo.FamilyMember;
import com.shah.pojo.Parent;
import com.shah.pojo.Student;

public class FamilyMemberDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(FamilyMember e) {
		int status=0;
		try {
			Connection con = FamilyMemberDao.getConnection();
		
			PreparedStatement ps = con.prepareStatement("insert into family_members("
					+ "parent_or_student_member,"
					+ "created_at,"
					+ "modified_at,"
					+ "created_by,"
					+ "modified_by,"
					+ "family_id,"
					+ "parents_id,"
					+ "students_id"
					+ ") values (?,?,?,?,?,?,?,?)");
			ps.setString(1, e.getName());
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(2, cr_dt_time);
			ps.setTimestamp(3, cr_dt_time);
			ps.setInt(4, e.getCreatedBy());
			ps.setInt(5, e.getModifiedBy());
			ps.setInt(6, e.getFamily().getId());
			ps.setInt(7, e.getParent().getId());
			ps.setInt(8, e.getStudent().getId());
						
			status = ps.executeUpdate();
		}
		catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
		
	}
	public static FamilyMember getFamilyMemberById(int id){  
	    FamilyMember e=new FamilyMember();      
	    try{  
	        Connection con=FamilyMemberDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from family_members where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        if(rs.next()){  
	        	e.setId(rs.getInt(1));
	        	e.setName(rs.getString(2));
	        	e.setFamily_id(rs.getInt(7));
	        	e.setParent_id(rs.getInt(8));
	        	e.setStudent_id(rs.getInt(9));
		                 }  
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return e;  
	}
	public static int update(FamilyMember e){  
	    int status=0;  
	    try{  
	        Connection con=FamilyMemberDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update family_members set parent_or_student_member=?,family_id=?,parents_id=?,students_id=? where id=?");  
	    	ps.setString(1, e.getName());
		    ps.setInt(2, e.getFamily_id());
			ps.setInt(3, e.getParent_id());
			ps.setInt(4, e.getStudent_id());
	        ps.setInt(5,e.getId());
			status = ps.executeUpdate();
	        con.close();  
	    }catch(Exception ex)
	    {ex.printStackTrace();}  
	    return status;  
	}

	public static int delete(int id){  
	    int status=0;
	    try{  
	        Connection con=FamilyMemberDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("delete from family_members where id=?");  
	        ps.setInt(1,id);  
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return status;  
	}
	public static List<FamilyMember> getAllFamilyMemberWithFamilyParentandStudentsName () {
		List<FamilyMember> list = new ArrayList<FamilyMember>();

		try {
			Connection con = FamilyMemberDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT fm.*,f.name,p.first_name,p.last_name,p.gender, st.first_name,st.last_name FROM school_management_system.family_members fm  LEFT OUTER JOIN famliies f ON f.id=fm.family_id LEFT OUTER JOIN parents p ON p.id=fm.parents_id LEFT OUTER JOIN students st ON fm.students_id=st.id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				FamilyMember e = new FamilyMember();
				e.setId(rs.getInt(1));
	            e.setName(rs.getString(2));
	            Family f = new Family();
	            f.setId(rs.getInt(7));
	            f.setName(rs.getString(11));
	            e.setFamily(f);
	            
	            
	            Parent p = new Parent();
	            p.setId(rs.getInt(8));
	            p.setFirstname(rs.getString(12));
	            p.setLastname(rs.getString(13));
	            e.setParent(p);

	            
	            Student a = new Student();
	            a.setId(rs.getInt(9));
	            a.setfirstname(rs.getString(15));
	            a.setlastname(rs.getString(16));
	            e.setStudent(a);
	            
	            		list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllParentAddrWithParentNames ");
		}

		return list;
	}
}

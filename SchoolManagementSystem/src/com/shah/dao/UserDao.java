package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shah.pojo.User;

public class UserDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(User e) {
		int status = 0;
		try {
			Connection con = UserDao.getConnection();

			PreparedStatement ps = con.prepareStatement(
					"insert into users("
					+ "username,"
					+ "password,"
					+ "firstname,"
					+ "lastname,"
					+ "gender,"
					+ "email,"
					+ "birthdate,"
					+ "phone_number,"
					+ "created_at,"
					+ "modified_at,"
					+ "created_by,"
					+ "modified_by,"
					+ "users_type_id"
					+ ") values "
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			
			
			ps.setString(1, e.getUsername());
			ps.setString(2, e.getPassword());
			ps.setString(3, e.getFirstname());
			ps.setString(4, e.getLastname());
			ps.setString(5, e.getGender());
			ps.setString(6, e.getEmail());

			//System.out.println("abc "+ e.getBirthdate());
			
			java.util.Date dob = new SimpleDateFormat("yyyy-dd-MM").parse(e.getBirthdate());

			//System.out.println(dob);

			java.sql.Date dt = new java.sql.Date(dob.getTime());

			ps.setDate(7, dt);
			ps.setString(8, e.getPhoneNumber());

			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());

			ps.setTimestamp(9, cr_dt_time);
			ps.setTimestamp(10, cr_dt_time);
			ps.setInt(11, e.getCreated_by());
			ps.setInt(12, e.getModified_by());
			ps.setInt(13, 6);

			status = ps.executeUpdate();
		} catch (Exception e2) {

			System.out.println(e2 + " error in database save methode");
		}
		return status;

	}

	public static int update(User e) {
		int status = 0;
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update users set username=?,password=?,firstname=?,lastname=?,gender=?,email=?,birthdate=?,phone_number=? where id=?");
			
			ps.setString(1, e.getUsername());
			ps.setString(2,e.getPassword());
			ps.setString(3, e.getFirstname());
			ps.setString(4, e.getLastname());
			ps.setString(5, e.getGender());
			ps.setString(6, e.getEmail());
			java.util.Date dob = new SimpleDateFormat("yyyy-dd-MM").parse(e.getBirthdate());
			java.sql.Date dt = new java.sql.Date(dob.getTime());
			ps.setDate(7, dt);

			ps.setString(8, e.getPhoneNumber());
			ps.setInt(9, e.getId());

			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;

	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from users where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static User getUserById(int id) {
		User e = new User();

		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from users where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setUsername(rs.getString(2));
				e.setPassword(rs.getString(3));
				e.setFirstname(rs.getString(4));
				e.setLastname(rs.getString(5));
				e.setGender(rs.getString(6));
				e.setEmail(rs.getString(7));
   		//java.util.Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(rs.getDate(6).getTime());
				Date dt = new Date(rs.getDate(8).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setBirthdate(dob.format(dt));
				e.setPhoneNumber(rs.getString(9));

			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}
	
	public static boolean isUsernameAvailable(String uname) {
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from users where username=?");
			ps.setString(1, uname);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}
	
	public static boolean isEmailAvailable(String email) {
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from users where email=?");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}
	
	public static User getUserByUnameNPassword(String uname,String psw,String status) {
		User e = new User();

		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from users where username=? AND password=? AND status=?");
			ps.setString(1, uname);
			ps.setString(2, psw);
			ps.setString(3, status);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setUsername(rs.getString(2));
				e.setPassword(rs.getString(3));
				e.setFirstname(rs.getString(4));
				e.setLastname(rs.getString(5));
				e.setGender(rs.getString(6));
				e.setEmail(rs.getString(7));
				Date dt = new Date(rs.getDate(8).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("MM/dd/yyyy");
            	e.setBirthdate(dob.format(dt));
				e.setPhoneNumber(rs.getString(9));

			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<User> getAllUsers() {
		List<User> list = new ArrayList<User>();

		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from users");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				User e = new User();
				e.setId(rs.getInt(1));
				e.setUsername(rs.getString(2));
				e.setPassword(rs.getString(3));
				e.setFirstname(rs.getString(4));
				e.setLastname(rs.getString(5));
				e.setGender(rs.getString(6));
				e.setEmail(rs.getString(7));
             	Date dt = new Date(rs.getDate(8).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setBirthdate(dob.format(dt));
				e.setPhoneNumber(rs.getString(9));

				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("error in All user method");
		}

		return list;
	}

}
package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Parent;
import com.shah.pojo.Student;
import com.shah.pojo.StudentParent;

public class StudentParentDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(StudentParent e) {
		int status = 0;
		try {
			Connection con = StudentParentDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into student_parents(" + "created_at," + "modified_at,"
					+ "created_by," + "modified_by," + "parents_id," + "students_id" + ") values (?,?,?,?,?,?)");
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(1, cr_dt_time);
			ps.setTimestamp(2, cr_dt_time);
			ps.setInt(3, e.getCreatedBy());
			ps.setInt(4, e.getModifiedBy());
			ps.setInt(5, e.getParent().getId());
			ps.setInt(6, e.getStudent().getId());

			status = ps.executeUpdate();
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;

	}

	public static StudentParent getStudentParentById(int id) {
		StudentParent e = new StudentParent();
		try {
			Connection con = StudentParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from student_parents where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setStudent_id(rs.getInt(6));
				e.setParent_id(rs.getInt(7));
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static int update(StudentParent e) {
		int status = 0;
		try {
			Connection con = StudentParentDao.getConnection();
			PreparedStatement ps = con
					.prepareStatement("update student_parents set students_id=?,parents_id=? where id=?");
			ps.setInt(1, e.getStudent_id());
			ps.setInt(2, e.getParent_id());

			ps.setInt(3, e.getId());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = StudentParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from student_parents where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static List<StudentParent> getAllStudentParentWithStudentsName() {
		List<StudentParent> list = new ArrayList<StudentParent>();

		try {
			Connection con = FamilyMemberDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT sp.*,st.first_name,st.last_name,p.first_name,p.last_name,p.gender  FROM school_management_system.student_parents sp  LEFT OUTER JOIN parents p ON p.id=sp.parents_id LEFT OUTER JOIN students st ON sp.students_id=st.id;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				StudentParent e = new StudentParent();
				e.setId(rs.getInt(1));
				
				
				Student a = new Student();
				a.setId(rs.getInt(6));
				a.setfirstname(rs.getString(9));
				a.setlastname(rs.getString(10));
				e.setStudent(a);
				
				Parent p = new Parent();
				p.setId(rs.getInt(7));
				p.setFirstname(rs.getString(11));
				p.setLastname(rs.getString(12));
				//p.setGender(rs.getString(13));
				e.setParent(p);

				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllParentAddrWithParentNames ");
		}

		return list;
	}
}

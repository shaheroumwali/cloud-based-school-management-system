package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.UserType;

public class UserTypeDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(UserType e) {
	int status=0;
	try {
	Connection con =UserTypeDao.getConnection();
	PreparedStatement ps = con.prepareStatement("insert into users_type("
			+ "type_name,"
			+ "description,"
			+ "created_at,"
			+ "modified_at,"
			+ "created_by,"
			+ "modified_by) values (?,?,?,?,?,?)");
	ps.setString(1,e.getUserType());
	ps.setString(2, e.getDescription());
	Date cr_dt = new Date();
	java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
	ps.setTimestamp(3, cr_dt_time);
	ps.setTimestamp(4, cr_dt_time);
	ps.setInt(5, 1);
	ps.setInt(6, 1);
	status=ps.executeUpdate();

	} catch (Exception e2) {
		System.out.println(e2 + " error in database save method");
	}
	return status;
}
	public static int update(UserType e){  
	    int status=0;  
	    try{  
	        Connection con=UserTypeDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update users_type set type_name=?,description=? where id=?");  
	        ps.setString(1, e.getUserType());
			ps.setString(2, e.getDescription());
			ps.setInt(3, e.getId());
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return status;  
	}
	public static int delete(int id){  
	    int status=0;  
	    try{  
	        Connection con=UserTypeDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("delete from users_type where id=?");  
	        ps.setInt(1,id);  
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return status;  
	}
	public static UserType getUserTypeById(int id){  
	    UserType e=new UserType();  
	      
	    try{  
	        Connection con=SchoolDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from users_type where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        if(rs.next()){  
	        	  e.setId(rs.getInt(1));
	        	  e.setUserType(rs.getString(2));
	        	  e.setDescription(rs.getString(3));
	        	         }  
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return e;  
	}
	public static List<UserType> getAllUserTye(){  
	    List<UserType> list=new ArrayList<UserType>();  
	      
	    try{  
	        Connection con=UserTypeDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from users_type");  
	        ResultSet rs=ps.executeQuery();  
	        while(rs.next()){  
	            UserType e=new UserType();  
	            e.setId(rs.getInt(1));
	        	  e.setUserType(rs.getString(2));
	        	  e.setDescription(rs.getString(3));
	        	  
	      	     list.add(e);  
	        }  
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return list;  
	}
}

package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.School;
import com.shah.pojo.Teacher;
public class TeacherDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(Teacher e) {
		int status = 0;
		try {
			Connection con = TeacherDao.getConnection();

			PreparedStatement ps = con.prepareStatement(
					"insert into teachers("
					+ "first_name,"
					+ "last_name,"
					+ "gender,"
					+ "other_teacher_details,"
					+ "created_at,"
					+ "modified_at,"
					+ "created_by,"
					+ "modified_by,"
					+ "schools_id"
					+ ") values "
					+ "(?,?,?,?,?,?,?,?,?)");
			ps.setString(1, e.getFirstname());
			ps.setString(2, e.getLastname());
			ps.setString(3, e.getGender());
			ps.setString(4, e.getOTD());
						Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(5, cr_dt_time);
			ps.setTimestamp(6, cr_dt_time);
			ps.setInt(7,1);
			ps.setInt(8,2);
			ps.setInt(9,e.getSchool().getId());

			status = ps.executeUpdate();
		} catch (Exception e2) {

			System.out.println(e2 + " error in database save methode");
		}
		return status;

	}
	public static int update(Teacher e) {
		int status = 0;
		try {
			Connection con = TeacherDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update teachers set first_name=?,last_name=?,gender=?,other_teacher_details=?,schools_id=? where id=?");
			
			ps.setString(1, e.getFirstname());
			ps.setString(2, e.getLastname());
			ps.setString(3, e.getGender());
			ps.setString(4, e.getOTD());
			ps.setInt(5, e.getSchools_id());
			ps.setInt(6, e.getId());
			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;

	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = TeacherDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from teachers where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static Teacher getTeacherById(int id) {
		Teacher e = new Teacher();

		try {
			Connection con = TeacherDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from teachers where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    e.setId(rs.getInt(1));
				e.setFirstname(rs.getString(2));
				e.setLastname(rs.getString(3));
				e.setGender(rs.getString(4));
				e.setOTD(rs.getString(5));
				e.setSchools_id(rs.getInt(11));
   		
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}
	public static List<Teacher> getAllTeachers() {
		List<Teacher> list = new ArrayList<Teacher>();

		try {
			Connection con = TeacherDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from teachers");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Teacher e = new Teacher();
				e.setId(rs.getInt(1));
				e.setFirstname(rs.getString(2));
				e.setLastname(rs.getString(3));
				e.setGender(rs.getString(4));
				e.setOTD(rs.getString(5));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("error in All Teacher method");
		}

		return list;
	}
	
	public static List<Teacher> getAllTeachersWithSchoolName() {
		List<Teacher> list = new ArrayList<Teacher>();

		try {
			Connection con = TeacherDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT pa.*,s.name FROM school_management_system.teachers pa LEFT OUTER JOIN schools s ON s.id=pa.schools_id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Teacher e = new Teacher();
				e.setId(rs.getInt(1));
				e.setFirstname(rs.getString(2));
				e.setLastname(rs.getString(3));
				e.setGender(rs.getString(4));
				e.setOTD(rs.getString(5));
				
				   School s = new School();
				s.setId(rs.getInt(11));
				s.setName(rs.getString(12));
				e.setSchool(s);
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("error in      getAllTeachersWithSchoolName");
		}

		return list;
	}
	

}

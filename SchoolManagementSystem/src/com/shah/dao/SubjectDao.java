package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shah.pojo.Subjects;

public class SubjectDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("  database error  " + ex);
		}
		return con;
	}
public static int save(Subjects e) {
	int status=0;
	try {
		Connection con = SubjectDao.getConnection();
	
		PreparedStatement ps = con.prepareStatement("insert into subjects("
				+ "subject_name,"
				+ "created_at,"
				+ "modified_at,"
				+ "created_by,"
				+ "modified_by"
				+ ") values (?,?,?,?,?)");
		ps.setString(1, e.getSubjecttName());
		
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(2, cr_dt_time);
		ps.setTimestamp(3, cr_dt_time);
		ps.setInt(4, e.getCreatedBy());
		ps.setInt(5, e.getModifiedBy());
		status = ps.executeUpdate();
	}
	catch (Exception e2) {
		System.out.println(e2 + " error in database save method");
	}
	return status;
	
}
public static int update(Subjects e){  
    int status=0;  
    try{  
        Connection con=SubjectDao.getConnection();  
        PreparedStatement ps=con.prepareStatement(  
        "update subjects set subject_name=? where id=?");  
        ps.setString(1,e.getSubjecttName());
        ps.setInt(2,e.getId());
		status = ps.executeUpdate();
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return status;   
}

public static int delete(int id){  
    int status=0;  
    try{  
        Connection con=SubjectDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("delete from subjects where id=?");  
        ps.setInt(1,id);  
        status=ps.executeUpdate();  
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return status;  
}
public static Subjects getSubjectypeById(int id){  
    Subjects e=new Subjects();      
    try{  
        Connection con=SubjectDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from subjects where id=?");  
        ps.setInt(1,id);  
        ResultSet rs=ps.executeQuery();  
        if(rs.next()){  
        	e.setId(rs.getInt(1));
        	e.setSubjecttName(rs.getString(2));
        	    
	                 }  
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return e;  
}
public static List<Subjects> getAllSubjects(){  
    List<Subjects> list=new ArrayList<Subjects>();  
      
    try{  
        Connection con=SubjectDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from subjects");  
        ResultSet rs=ps.executeQuery();  
        while(rs.next()){  
            Subjects e=new Subjects();  
      	  e.setId(rs.getInt(1));
          e.setSubjecttName(rs.getString(2));
         list.add(e);  
        }  
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return list;  
}
}

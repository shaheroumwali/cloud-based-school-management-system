package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Addresses;
import com.shah.pojo.Parent;
import com.shah.pojo.ParentAddress;

public class ParentAddressDao {

	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(ParentAddress e) {
		int status = 0;
		try {
			Connection con = ParentAddressDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into parent_addresses(" + "date_address_from,"
					+ "date_address_to," + "created_at," + "modified_at," + "created_by," + "modified_by,"
					+ "addresses_id," + "parents_id" + ") values (?,?,?,?,?,?,?,?)");
			java.util.Date dF = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressFrom());
			java.sql.Date df = new java.sql.Date(dF.getTime());
			ps.setDate(1, df);
			java.util.Date dT = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressTo());
			java.sql.Date dt = new java.sql.Date(dT.getTime());
			ps.setDate(2, dt);
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(3, cr_dt_time);
			ps.setTimestamp(4, cr_dt_time);
			ps.setInt(5, 2);
			ps.setInt(6, 2);
			ps.setInt(7, e.getAddresses().getId());
			ps.setInt(8, e.getParent().getId());

//		System.out.println(e.getParent().getId());

			status = ps.executeUpdate();
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}

public static int update(ParentAddress e) {
	int status = 0;
	try {
		Connection con = ParentAddressDao.getConnection();
		PreparedStatement ps = con.prepareStatement(
				"update parent_addresses set date_address_from=?,date_address_to=?,parents_id=?,addresses_id=? where id=?");
		
		ps.setString(1,e.getDateAddressFrom());
        ps.setString(2,e.getDateAddressTo());
		ps.setInt(3, e.getParent_id());	
		ps.setInt(4,e.getAddresses_id());
		ps.setInt(5,e.getId());
		status = ps.executeUpdate();
	

		con.close();
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	return status;

}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = ParentAddressDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from parent_addresses where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static ParentAddress getParentAddressById(int id) {
		ParentAddress e = new ParentAddress();

		try {
			Connection con = ParentAddressDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from parent_addresses where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setDateAddress_from(rs.getString(2));
				e.setDateddressT(rs.getString(3));
				e.setParent_id(rs.getInt(8));
				e.setAddresses_id(rs.getInt(9));
				// e.setId(id);
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<ParentAddress> getAllParentAddresses() {
		List<ParentAddress> list = new ArrayList<ParentAddress>();

		try {
			Connection con = ParentAddressDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from parent_addresses");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ParentAddress e = new ParentAddress();
				e.setDateAddress_from(rs.getString(1));
				e.setDateddressT(rs.getString(2));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("error in All Parent method");
		}

		return list;
	}

	public static List<ParentAddress> getAllParentAddrWithParentNames() {
		List<ParentAddress> list = new ArrayList<ParentAddress>();

		try {
			Connection con = ParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT pa.*,p.first_name,p.last_name,p.gender,p.other_parent_details,p.`status`, addr.address_details FROM school_management_system.parent_addresses pa LEFT OUTER JOIN parents p ON p.id=pa.parents_id LEFT OUTER JOIN addresses addr ON pa.addresses_id=addr.id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				ParentAddress e = new ParentAddress();
				e.setId(rs.getInt(1));
				e.setDateAddress_from(rs.getString(2));
				e.setDateddressT(rs.getString(3));

				Parent p = new Parent();
				p.setId(rs.getInt(9));
				p.setFirstname(rs.getString(11));
				p.setLastname(rs.getString(12));
				p.setGender(rs.getString(13));
				p.setOPD(rs.getString(14));
				p.setStatus(rs.getString(15));
				e.setParent(p);

				Addresses a = new Addresses();
				a.setId(rs.getInt(8));
				a.setAddressDetails(rs.getString(16));
				e.setAddresses(a);

				// e.setParent(rs.get);
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllParentAddrWithParentNames ");
		}

		return list;
	}
}
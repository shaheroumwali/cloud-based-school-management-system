package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Homework;
import com.shah.pojo.Student;

public class HomeworkDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(Homework e) {
		int status=0;
		try {
		Connection con =HomeworkDao.getConnection();
		PreparedStatement ps = con.prepareStatement("insert into homework("
				+ "date_created,"
				+ "homework_content,"
				+ "grade,"
				+ "other_homework_deatails,"
				+ "created_by,"
				+ "modified_by,"
				+ "created_at,"
				+ "modified_at,"
				+ "students_id) values (?,?,?,?,?,?,?,?,?)");
		java.util.Date dF = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateCreated());
		java.sql.Date df = new java.sql.Date(dF.getTime());
		ps.setDate(1, df);	
		ps.setString(2,e.getHomeworkContent());
		ps.setInt(3,e.getGrade());
		System.out.println("Homework grade is  " + (e.getGrade()));
		ps.setString(4, e.getOtherhomeworkDetail());
		System.out.println("OtherHomework Detail is  " + (e.getOtherhomeworkDetail()));

		ps.setInt(5,1);
		ps.setInt(6, 1);
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(7, cr_dt_time);
		ps.setTimestamp(8, cr_dt_time);
		ps.setInt(9, e.getStudent().getId());
		status=ps.executeUpdate();

		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}

	public static int update(Homework e) {
		int status = 0;
		try {
			Connection con = HomeworkDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update homework set date_created=?,homework_content=?,grade=?,other_homework_deatails=?,students_id=? where id=?");
			
			ps.setString(1,e.getDateCreated());
			ps.setString(2, e.getHomeworkContent());
			ps.setInt(3, e.getGrade());
			ps.setString(4, e.getOtherhomeworkDetail());
			ps.setInt(5, e.getStudents_id());
		    ps.setInt(6, e.getId());
			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			System.out.println("error in dbs updte methode" + ex);
		}
		return status;

	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = HomeworkDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from homework where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static Homework getHomeworkById(int id) {
		Homework e = new Homework();

		try {
			Connection con = HomeworkDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from homework where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    e.setId(rs.getInt(1));
			    e.setDateCreated(rs.getString(2));
			    e.setHomeworkContent(rs.getString(3));
			    e.setGrade(rs.getInt(4));
			    e.setOtherhomeworkDetail(rs.getString(5));
			    e.setStudents_id(rs.getInt(10));
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}
	public static List<Homework> getAllHomework() {
		List<Homework> list = new ArrayList<Homework>();

		try {
			Connection con = HomeworkDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from homework");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Homework e = new Homework();
				e.setId(rs.getInt(1));
				e.setDateCreated(rs.getString(2));
			    e.setHomeworkContent(rs.getString(3));
			    e.setGrade(rs.getInt(4));
			    e.setOtherhomeworkDetail(rs.getString(5));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
		}

		return list;
	}
	public static List<Homework> getAllHomeworkWithStudentName () {
		List<Homework> list = new ArrayList<Homework>();

		try {
			Connection con = HomeworkDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT H.*, st.first_name,st.last_name FROM school_management_system.homework H  LEFT OUTER JOIN students st ON H.students_id=st.id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Homework e = new Homework();
				e.setId(rs.getInt(1));
				e.setDateCreated(rs.getString(2));
			    e.setHomeworkContent(rs.getString(3));
			    e.setGrade(rs.getInt(4));
			    e.setOtherhomeworkDetail(rs.getString(5));
				            Student a = new Student();
	            a.setId(rs.getInt(10));
	            a.setfirstname(rs.getString(12));
	            a.setlastname(rs.getString(13));
	            e.setStudent(a);
	            
	            		list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllHomewworkWithStudentName ");
		}

		return list;
	}

}

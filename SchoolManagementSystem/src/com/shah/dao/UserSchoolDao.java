package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.School;
import com.shah.pojo.User;
import com.shah.pojo.UserSchool;

public class UserSchoolDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(UserSchool e) {
		int status=0;
		try {
			Connection con = UserSchoolDao.getConnection();
		
			PreparedStatement ps = con.prepareStatement("insert into users_schools("
					+"users_id,"
					+ "schools_id,"
					+ "created_at,"
					+ "modified_at,"
					+ "created_by,"	
					+ "modified_by"
					+ ") values (?,?,?,?,?,?)");
			
			ps.setInt(1,e.getUser().getId());
			ps.setInt(2,e.getSchool().getId());
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(3, cr_dt_time);
			ps.setTimestamp(4, cr_dt_time);
			ps.setInt(5, 2);
			ps.setInt(6, 2);
		
			
//			System.out.println(e.getParent().getId());
			
			status = ps.executeUpdate();
		}
		catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}
	public static UserSchool getUserSchoolById(int id){  
		UserSchool e=new UserSchool();      
	    try{  
	        Connection con=UserSchoolDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from users_schools where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        if(rs.next()){  
	        	e.setId(rs.getInt(1));
	        	e.setUser_id(rs.getInt(2));
	        	e.setSchool_id(rs.getInt(3));
	        	
	        	
		                 }  
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return e;  
	}
	public static int update(UserSchool e){  
	    int status=0;  
	    try{  
	        Connection con=UserSchoolDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update users_schools set users_id=?,schools_id=? where id=?");  
	        ps.setInt(1, e.getUser_id());
	    	ps.setInt(2,e.getSchool_id());
	        ps.setInt(3,e.getId());
			status = ps.executeUpdate();
	        con.close();  
	    }catch(Exception ex)
	    {ex.printStackTrace();}  
	    return status;  
	}

	public static int delete(int id){  
	    int status=0;
	    try{  
	        Connection con=UserSchoolDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("delete from users_schools where id=?");  
	        ps.setInt(1,id);  
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return status;  
	}
	public static List<UserSchool> getAllUserSchoolWithSchoolNameandAddrs () {
		List<UserSchool> list = new ArrayList<UserSchool>();

		try {
			Connection con = UserSchoolDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT Usersch.*,sch.name,sch.school_principal,Usr.firstname,Usr.lastname FROM school_management_system.users_schools Usersch  LEFT OUTER JOIN schools sch ON sch.id=Usersch.schools_id LEFT OUTER JOIN  users Usr ON Usersch.users_id=Usr.id;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserSchool e = new UserSchool();
				e.setId(rs.getInt(1));
				
				
				School s = new School();
				s.setId(rs.getInt(3));
				s.setName(rs.getString(9));
				s.setPrincipalName(rs.getString(10));
				e.setSchool(s);
				
				User Usr = new User();
				Usr.setId(rs.getInt(2));
				Usr.setFirstname(rs.getString(11));
				Usr.setLastname(rs.getString(12));
				//addr.setAddressDetails(rs.getString(11));
				e.setUser(Usr);
	           
	            		list.add(e);
			}
			con.close();
		} catch (Exception e) {
			
			System.out.println("Error In getAllSchoolAddrWithSchoolNameANDAddresses ");
		}

		return list;
	}



}

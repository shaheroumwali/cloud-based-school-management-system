package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Subjects;
import com.shah.pojo.Teacher;

public class ClassDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(com.shah.pojo.Class e) {
		int status = 0;
		try {
			Connection con = ClassDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into classes(" + "code," + "name," + "date_from,"
					+ "date_to," + "created_at," + "modified_at," + "created_by," + "modified_by," + "teachers_id,"
					+ "subjects_id" + ") values (?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, e.getCode());
			ps.setString(2, e.getName());
			java.util.Date dF = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressFrom());
			java.sql.Date df = new java.sql.Date(dF.getTime());
			ps.setDate(3, df);
			java.util.Date dT = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressTo());
			java.sql.Date dt = new java.sql.Date(dT.getTime());
			ps.setDate(4, dt);
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(5, cr_dt_time);
			ps.setTimestamp(6, cr_dt_time);
			ps.setInt(7, 2);
			ps.setInt(8, 2);
			ps.setInt(9, e.getTeacher().getId());
			ps.setInt(10, e.getSubject().getId());
			// ps.setInt(9, 1);
			// ps.setInt(10, 1);
//		System.out.println(e.getParent().getId());

			status = ps.executeUpdate();
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}
	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = ClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from classes where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}
	public static com.shah.pojo.Class getClassById(int id) {
		com.shah.pojo.Class e = new com.shah.pojo.Class();

		try {
			Connection con = ClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from classes where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setCode(rs.getString(2));
				e.setName(rs.getString(3));
				e.setDateAddressFrom(rs.getString(4));
				e.setDateAddressTo(rs.getString(5));
				e.setTeacher_id(rs.getInt(10));
				e.setSubject_id(rs.getInt(11));

			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static int update(com.shah.pojo.Class e){  
	    int status=0;  
	    try{  
	        Connection con=ClassDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update classes set code=?,name=?,date_from=?,date_to=?,teachers_id=?,subjects_id=? where id=?");  
	        ps.setString(1,e.getCode());
	        ps.setString(2,e.getName());
	        ps.setString(3,e.getDateAddressFrom());
	        ps.setString(4, e.getDateAddressTo());
	        ps.setInt(5, e.getTeacher_id());
	        ps.setInt(6, e.getSubject_id());
	        ps.setInt(7,e.getId());
			status = ps.executeUpdate();
	        con.close();  
	    }catch(Exception ex)
	    {ex.printStackTrace();}  
	    return status;  
	}
	public static List<com.shah.pojo.Class> getAllClasses() {
		List<com.shah.pojo.Class> list = new ArrayList<com.shah.pojo.Class>();

		try {
			Connection con = ClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from classes");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				com.shah.pojo.Class e = new com.shah.pojo.Class();
				e.setId(rs.getInt(1));
				e.setCode(rs.getString(2));
				e.setName(rs.getString(3));
				e.setDateAddressFrom(rs.getString(4));
				e.setDateAddressTo(rs.getString(5));
				list.add(e);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public static List<com.shah.pojo.Class> getAllClassesWithTeacherAndSubjectName() {
		List<com.shah.pojo.Class> list = new ArrayList<com.shah.pojo.Class>();

		try {
			Connection con = ClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT pa.*,t.first_name,t.last_name, s.subject_name FROM school_management_system.classes pa LEFT OUTER JOIN teachers t ON t.id=pa.teachers_id LEFT OUTER JOIN subjects s ON pa.subjects_id=s.id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				com.shah.pojo.Class e = new com.shah.pojo.Class();
				e.setId(rs.getInt(1));
				e.setCode(rs.getString(2));
				e.setName(rs.getString(3));
				e.setDateAddressFrom(rs.getString(4));
				e.setDateAddressTo(rs.getString(5));

				Teacher t = new Teacher();
				t.setId(rs.getInt(10));
				t.setFirstname(rs.getString(13));
				t.setLastname(rs.getString(14));
				e.setTeacher(t);
				;
				Subjects s = new Subjects();
				s.setId(rs.getInt(11));
				s.setSubjecttName(rs.getString(15));
				e.setSubject(s);
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllClass Record ");
		}

		return list;
	}
}

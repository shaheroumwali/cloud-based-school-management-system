package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shah.pojo.PayementType;



public class PayementTypeDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(PayementType e) {
		int status = 0;
		try {
			Connection con = PayementTypeDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into payement_type(" + "name," + "created_at,"
					+ "modified_at," + "created_by," + "modified_by" + ") values (?,?,?,?,?)");
			ps.setString(1, e.getName());

			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(2, cr_dt_time);
			ps.setTimestamp(3, cr_dt_time);
			ps.setInt(4, e.getCreatedBy());
			ps.setInt(5, e.getModifiedBy());
			status = ps.executeUpdate();
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;

	}

	public static int update(PayementType e) {
		int status = 0;
		try {
			Connection con = PayementTypeDao.getConnection();
			PreparedStatement ps = con.prepareStatement("update payement_type set name=? where id=?");
			ps.setString(1, e.getName());
			ps.setInt(2, e.getId());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = PayementTypeDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from payement_type where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static PayementType getPayemenTypeById(int id) {
		PayementType e = new PayementType();
		try {
			Connection con = PayementTypeDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from payement_type where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setName(rs.getString(2));

			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<PayementType> getAllPayementType() {
		List<PayementType> list = new ArrayList<PayementType>();

		try {
			Connection con = PayementTypeDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from payement_type");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				PayementType e = new PayementType();
				e.setId(rs.getInt(1));
				e.setName(rs.getString(2));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
}
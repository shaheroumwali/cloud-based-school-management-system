package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.School;
import com.shah.pojo.Student;
import com.shah.pojo.StudentAddress;
import com.shah.pojo.StudentClasses;

public class StudentDao {

	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");

		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(Student e) {
		int status = 0;
		try {
			Connection con = StudentDao.getConnection();

			PreparedStatement ps = con.prepareStatement(
					"insert into students(" + "rollno," + "gender," + "first_name," + "last_name," + "date_of_birth,"
							+ "tuition_fee," + "other_student_details," + "created_at," + "modified_at," + "created_by,"
							+ "modified_by," + "schools_id" + ") values (?,?,?,?,?,?,?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, e.getRollno());
			ps.setString(2, e.getgender());
			ps.setString(3, e.getfirstname());
			ps.setString(4, e.getlastname());

			java.util.Date dob = new SimpleDateFormat("yyyy-dd-MM").parse(e.getDOB());
			java.sql.Date dt = new java.sql.Date(dob.getTime());

			ps.setDate(5, dt);
			ps.setDouble(6, e.getTuition_fee());
			ps.setString(7, e.getOther_student_detail());

			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());

			ps.setTimestamp(8, cr_dt_time);
			ps.setTimestamp(9, cr_dt_time);

			ps.setInt(10, e.getCreated_by());
			ps.setInt(11, e.getModified_by());
			ps.setInt(12, e.getSchool().getId());

			status = ps.executeUpdate();
			int stdId = -1;

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next()) {
				stdId = rs.getInt(1);
			}
			rs.close();
			ps.close();

			ps = con.prepareStatement("insert into student_classes(" + "date_from," + "date_to," + "created_at,"
					+ "modified_at," + "created_by," + "modified_by," + "classes_id," + "students_id"
					+ ") values (?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			java.util.Date dF = new SimpleDateFormat("YYYY-MM-DD").parse(e.getStudentClasses().getDateAddressFrom());
			java.sql.Date df = new java.sql.Date(dF.getTime());
			ps.setDate(1, df);
			java.util.Date dT = new SimpleDateFormat("YYYY-MM-DD").parse(e.getStudentClasses().getDateAddressTo());
			dt = new java.sql.Date(dT.getTime());
			ps.setDate(2, dt);
			ps.setTimestamp(3, cr_dt_time);
			ps.setTimestamp(4, cr_dt_time);
			ps.setInt(5, 2);
			ps.setInt(6, 2);
			ps.setInt(7, e.getStudentClasses().getClasses().getId());
			ps.setInt(8, stdId);
			status = ps.executeUpdate();
			ps.close();

			ps = con.prepareStatement("insert into addresses(address_details," + "created_at," + "modified_at,"
					+ "created_by," + "modified_by) values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, e.getStudentAddress().getAddressDetail());
			ps.setTimestamp(2, cr_dt_time);
			ps.setTimestamp(3, cr_dt_time);
			ps.setInt(4, 2);
			ps.setInt(5, 2);
			status = ps.executeUpdate();

			int addrId = -1;

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				addrId = rs.getInt(1);
			}
			rs.close();
			ps.close();

			ps = con.prepareStatement("insert into student_addresses(" + "date_from," + "date_to," + "address_details,"
					+ "created_at," + "modified_at," + "created_by," + "modified_by," + "addresses_id," + "students_id"
					+ ") values (?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			java.util.Date d = new SimpleDateFormat("YYYY-MM-DD").parse(e.getStudentAddress().getDateAddressFrom());
			java.sql.Date de = new java.sql.Date(d.getTime());
			ps.setDate(1, de);
			java.util.Date ds = new SimpleDateFormat("YYYY-MM-DD").parse(e.getStudentAddress().getDateAddressTo());
			dt = new java.sql.Date(ds.getTime());
			ps.setDate(2, dt);
			ps.setString(3, e.getStudentAddress().getAddressDetail());
			// Date cr_dt = new Date();
			// ava.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(4, cr_dt_time);
			ps.setTimestamp(5, cr_dt_time);
			ps.setInt(6, 0);
			ps.setInt(7, 0);
			ps.setInt(8, addrId);
			ps.setInt(9, stdId);
			status = ps.executeUpdate();

		} catch (Exception e2) {
			e2.printStackTrace();

//			System.out.println();
			System.out.println(e2 + "error in database save methode");
		}
		return status;

	}

	public static int update(Student e) {
		int status = 0;
		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("update students set rollno="
					+ "?,gender=?,first_name=?,last_name=?,date_of_birth=?,tuition_fee=?,other_student_details=? where id=?");

			ps.setInt(1, e.getRollno());
			ps.setString(2, e.getgender());
			ps.setString(3, e.getfirstname());
			ps.setString(4, e.getlastname());
			java.util.Date dob = new SimpleDateFormat("yyyy-dd-MM").parse(e.getDOB());
			java.sql.Date dt = new java.sql.Date(dob.getTime());
			ps.setDate(5, dt);
			ps.setDouble(6, e.getTuition_fee());
			ps.setString(7, e.getOther_student_detail());
//			ps.setInt(8, e.getSchool_id());
			ps.setInt(8, e.getId());
			System.out.println(ps.toString());

			status = ps.executeUpdate();
//			int stdId = -1;
//
//			ResultSet rs = ps.getGeneratedKeys();
//
//			if (rs.next()) {
//				stdId = rs.getInt(1);
//			}
//			rs.close();
			ps.close();

			ps = con.prepareStatement(
					"update student_classes set" + "date_from=?," + "date_to=?," + "classes_id=? where  students_id=?",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, e.getStudentClasses().getDateAddressFrom());
			ps.setString(2, e.getStudentClasses().getDateAddressTo());
			ps.setInt(3, e.getStudentClasses_id());
			ps.setInt(4, e.getStudentAddress().getStudent_id());

			status = ps.executeUpdate();

			;
			ps.close();

			ps = con.prepareStatement("update student_addresses set" + "date_from=?," + "date_to=?,"
					+ "address_details=? where  students_id=?", Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, e.getStudentAddress().getDateAddressFrom());
			ps.setString(2, e.getStudentAddress().getDateAddressTo());
			ps.setString(3, e.getStudentAddress().getAddressDetail());
			ps.setInt(4, e.getStudentAddress().getStudent_id());
			status = ps.executeUpdate();

			ps.close();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from students where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static Student getStudentById(int student_id) {
		Student e = new Student();

		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from students where id=?");
			ps.setInt(1, student_id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
//				System.out.println("I am here");
				e.setId(rs.getInt(1));

//				System.out.println("Student id is  " + rs.getInt(1));

				e.setRollno(rs.getInt(2));
				e.setgender(rs.getString(3));
				e.setfirstname(rs.getString(4));
				e.setlastname(rs.getString(5));

//				e.setSchool(school);

//			   		java.util.Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(rs.getDate(6).getTime());
				Date dt = new Date(rs.getDate(6).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setDOB(dob.format(dt));
				e.setTuition_fee(rs.getDouble(7));
				e.setOther_student_detail(rs.getString(8));
				e.setSchool_id(rs.getInt(13));

			}
			ps.close();
			rs.close();
			ps = con.prepareStatement("SELECT * FROM student_classes where students_id=?");
			ps.setInt(1, student_id);
			rs = ps.executeQuery();

			StudentClasses stdclasses = new StudentClasses();
			if (rs.next()) {

				stdclasses.setDateAddressFrom(rs.getDate(2).toString());
				stdclasses.setDateAddressTo(rs.getDate(4).toString());
				stdclasses.setClass_id(rs.getInt(8));
//				System.out.println("Student id is  " + rs.getInt(1));

			}

			e.setStudentClasses(stdclasses);

			ps.close();
			rs.close();
			ps = con.prepareStatement("SELECT * FROM student_addresses where students_id=?");
			ps.setInt(1, student_id);
			rs = ps.executeQuery();

			StudentAddress stdAddresses = new StudentAddress();
			if (rs.next()) {

				stdAddresses.setDateAddressFrom(rs.getDate(2).toString());
				stdAddresses.setDateAddressTo(rs.getDate(3).toString());
				stdAddresses.setAddressDetail(rs.getString(4));
//				System.out.println("Student id is  " + rs.getInt(1));

			}

			e.setStudentAddress(stdAddresses);

//			ps = con.prepareStatement("SELECT * FROM classes where id=?");
//			ps.setInt(1, student_id);
//			rs = ps.executeQuery();
//
//			com.shah.pojo.Class clas = new com.shah.pojo.Class();
//			if (rs.next()) {
//				clas.setCode(rs.getString(2));
//				clas.setName(rs.getString(3));
//				clas.setDateAddressFrom(rs.getDate(4).toString());
//				clas.setDateAddressTo(rs.getDate(4).toString());
//				Clas.setClass_id(rs.getInt(8));
////				System.out.println("Student id is  " + rs.getInt(1));
//
//			
//
//			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<Student> getAllStudents() {
		List<Student> list = new ArrayList<Student>();

		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from students");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Student e = new Student();
				e.setId(rs.getInt(1));
				e.setRollno(rs.getInt(2));
				e.setgender(rs.getString(3));
				e.setfirstname(rs.getString(4));
				e.setlastname(rs.getString(5));
				Date dt = new Date(rs.getDate(6).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setDOB(dob.format(dt));
				e.setTuition_fee(rs.getDouble(7));
				e.setOther_student_detail(rs.getString(8));

				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public static List<Student> getAllStudentWithSchoolName() {
		List<Student> list = new ArrayList<Student>();

		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT st.*,sc.name FROM school_management_system.students st LEFT OUTER JOIN schools sc ON sc.id=st.schools_id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Student e = new Student();
				e.setId(rs.getInt(1));
				e.setRollno(rs.getInt(2));
				e.setgender(rs.getString(3));
				e.setfirstname(rs.getString(4));
				e.setlastname(rs.getString(5));

//		   		java.util.Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(rs.getDate(6).getTime());
				Date dt = new Date(rs.getDate(6).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setDOB(dob.format(dt));
				e.setTuition_fee(rs.getDouble(7));
				e.setOther_student_detail(rs.getString(8));
				School s = new School();
				s.setId(rs.getInt(13));
				s.setName(rs.getString(15));
				e.setSchool(s);

				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public static List<Student> getAllStudentBySchoolId(int schoolId) {
		List<Student> list = new ArrayList<Student>();

		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT st.*,sc.name FROM school_management_system.students st LEFT  OUTER JOIN schools sc ON sc.id=st.schools_id WHERE st.schools_id=?");
			ps.setInt(1, schoolId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Student e = new Student();
				e.setId(rs.getInt(1));
				e.setRollno(rs.getInt(2));
				e.setgender(rs.getString(3));
				e.setfirstname(rs.getString(4));
				e.setlastname(rs.getString(5));

//		   		java.util.Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(rs.getDate(6).getTime());
				Date dt = new Date(rs.getDate(6).getTime());
				SimpleDateFormat dob = new SimpleDateFormat("yyyy-dd-MM");
				e.setDOB(dob.format(dt));
				e.setTuition_fee(rs.getDouble(7));
				e.setOther_student_detail(rs.getString(8));
				School s = new School();
				s.setId(rs.getInt(13));
				s.setName(rs.getString(15));
				e.setSchool(s);

				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	public static boolean isRollNoAvailable(String rollNo) {
		try {
			Connection con = StudentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from students where rollno=?");
			ps.setString(1,rollNo);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

}

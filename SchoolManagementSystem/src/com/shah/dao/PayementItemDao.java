package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.shah.pojo.PayementItem;
import com.shah.pojo.Payements;

public class PayementItemDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(PayementItem e) {
		int status = 0;
		try {
			Connection con = PayementItemDao.getConnection();

			PreparedStatement ps = con.prepareStatement(
					"insert into payement_items(" + "description," + "amount," + "created_at," + "modified_at,"
							+ "created_by," + "modified_by," + "payements_id" + ") values (?,?,?,?,?,?,?)");

			ps.setString(1, e.getDescription());
			ps.setDouble(2, e.getAmount());
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(3, cr_dt_time);
			ps.setTimestamp(4, cr_dt_time);
			ps.setInt(5, e.getCreatedBy());
			ps.setInt(6, e.getModifiedBy());
			ps.setInt(7, e.getPayement().getId());

			status = ps.executeUpdate();
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;

	}

	public static int update(PayementItem e) {
		int status = 0;
		try {
			Connection con = PayementItemDao.getConnection();
			PreparedStatement ps = con
					.prepareStatement("update payement_items set description=?,amount=?,payements_id=? where id=?");
			ps.setString(1, e.getDescription());
			ps.setDouble(2, e.getAmount());
			ps.setInt(3, e.getPayement_id());
			ps.setInt(4, e.getId());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = PayementItemDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from payement_items where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static PayementItem getPayementItemById(int id) {
		PayementItem e = new PayementItem();
		try {
			Connection con = PayementItemDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from payement_items where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setDescription(rs.getString(2));
				e.setAmount(rs.getDouble(3));
				e.setPayement_id(rs.getInt(8));
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<PayementItem> getAllPayementItem() {
		List<PayementItem> list = new ArrayList<PayementItem>();

		try {
			Connection con = PayementItemDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from payement_items");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				PayementItem e = new PayementItem();
				e.setId(rs.getInt(1));
				e.setDescription(rs.getString(2));
				e.setAmount(rs.getDouble(3));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}


	public static List<PayementItem> getAllPayementItemWithPayements() {
		List<PayementItem> list = new ArrayList<PayementItem>();

		try {
			Connection con = PayementItemDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT PayItem.*,payment.amount,payment.payement_date FROM school_management_system.payement_items PayItem LEFT OUTER JOIN payements payment ON payment.id=PayItem.payements_id;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				PayementItem e = new PayementItem();
				e.setId(rs.getInt(1));
				e.setDescription(rs.getString(2));
				e.setAmount(rs.getDouble(3));
				Payements payement = new Payements();
				payement.setId(rs.getInt(8));
				payement.setAmount(rs.getDouble(10));
				payement.setPayDate(rs.getString(11));
				e.setPayement(payement);
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	
}

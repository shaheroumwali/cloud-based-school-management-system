package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import java.util.List;

import com.shah.pojo.Addresses;


public class addressesDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(Addresses e) {
		int status = 0;
		try {
			Connection con = addressesDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into addresses(address_details) values (?)");
			ps.setString(1, e.getAddressDetails());
			

			status = ps.executeUpdate();
		} catch (Exception e2) {

			System.out.println(e2 + " error in database save method ");
		}
		return status;

	}

	public static int update(Addresses e) {
		int status = 0;
		try {
			Connection con = addressesDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update addresses set address_details=? where id=?");
			ps.setString(1, e.getAddressDetails());
			ps.setInt(2, e.getId());
			
						status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;

	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = addressesDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from addresses where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static Addresses getAddressById(int id) {
		Addresses e = new Addresses();

		try {
			Connection con = addressesDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from addresses where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setAddressDetails(rs.getString(2));		
				
				}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static List<Addresses> getAllAddresses() {
		List<Addresses> list = new ArrayList<Addresses>();

		try {
			Connection con = addressesDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from addresses");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Addresses e = new Addresses();
					e.setId(rs.getInt(1));
					e.setAddressDetails(rs.getString(2));		
					
				
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
}

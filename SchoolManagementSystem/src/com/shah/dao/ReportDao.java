package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Report;
import com.shah.pojo.Student;

public class ReportDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(Report e) {
		int status=0;
		try {
		Connection con =ReportDao.getConnection();
		PreparedStatement ps = con.prepareStatement("insert into reports("
				+ "date_created,"
				+ "report_content,"
				+ "teacher_comments,"
				+ "other_report_details,"
				+ "created_by,"
				+ "modified_by,"
				+ "created_at,"
				+ "modified_at,"
				+ "students_id"
				+ ") values (?,?,?,?,?,?,?,?,?)");
		ps.setString(1,e.getDateCreated());
		ps.setString(2, e.getReportContent());
		ps.setString(3, e.getTeacherComment());
		ps.setString(4, e.getOtherReportDetail());
		ps.setInt(5, 1);
		ps.setInt(6,1);
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(7, cr_dt_time);
		ps.setTimestamp(8, cr_dt_time);
		ps.setInt(9,e.getStudent().getId());
		status=ps.executeUpdate();

		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}
	
	public static int update(Report e){  
	    int status=0;  
	    try{  
	        Connection con=ReportDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement(  
	        "update reports set  date_created=?,report_content=?,teacher_comments=?,other_report_details=?, students_id=?  where id=?");  
	        ps.setString(1,e.getDateCreated());
	        ps.setString(2,e.getReportContent());
	        ps.setString(3,e.getTeacherComment());
	        ps.setString(4, e.getOtherReportDetail());    
	        ps.setInt(5,e.getStudent_id());
	        ps.setInt(6,e.getId());
			status = ps.executeUpdate();
	        con.close();  
	    }catch(Exception ex)
	    {ex.printStackTrace();}  
	    return status;  
	}

	public static int delete(int id){  
	    int status=0;
	    try{  
	        Connection con=ReportDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("delete from reports where id=?");  
	        ps.setInt(1,id);  
	        status=ps.executeUpdate();  
	          
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return status;  
	}
	public static Report getReportById(int id){  
	    Report e=new Report();      
	    try{  
	        Connection con=ReportDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from reports where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        if(rs.next()){  
	        	e.setId(rs.getInt(1));
	        	e.setDateCreated(rs.getString(2));
	        	e.setReportContent(rs.getString(3));
	        	e.setTeacherComment(rs.getString(4));
	        	e.setOtherReportDetail(rs.getString(5));
	        	e.setStudent_id(rs.getInt(10));;
	        	
	        	    
		                 }  
	        con.close();  
	    }catch(Exception ex){ex.printStackTrace();}  
	      
	    return e;  
	}
	public static List<Report> getAllReport(){  
	    List<Report> list=new ArrayList<Report>();  
	      
	    try{  
	        Connection con=ReportDao.getConnection();  
	        PreparedStatement ps=con.prepareStatement("select * from reports");  
	        ResultSet rs=ps.executeQuery();  
	        while(rs.next()){  
	            Report e=new Report();  
	            e.setId(rs.getInt(1));
	        	e.setDateCreated(rs.getString(2));
	        	e.setReportContent(rs.getString(3));
	        	e.setTeacherComment(rs.getString(4));
	        	e.setOtherReportDetail(rs.getString(5));
	         list.add(e);  
	        }  
	        con.close();  
	    }catch(Exception e){e.printStackTrace();}  
	      
	    return list;  
	}
	public static List<Report> getAllReportWithStudentName () {
		List<Report> list = new ArrayList<Report>();

		try {
			Connection con = ReportDao.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT r.*, st.first_name,st.last_name FROM school_management_system.reports r  LEFT OUTER JOIN students st ON r.students_id=st.id");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Report e = new Report();
				e.setId(rs.getInt(1));
				e.setDateCreated(rs.getString(2));
			    e.setReportContent(rs.getString(3));
			    e.setTeacherComment(rs.getString(4));
			    e.setOtherReportDetail(rs.getString(5));
			    
				            Student a = new Student();
	            a.setId(rs.getInt(10));
	            a.setfirstname(rs.getString(12));
	            a.setlastname(rs.getString(13));
	            e.setStudent(a);
	            
	            		list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllReportWithStudentName ");
		}

		return list;
	}


}

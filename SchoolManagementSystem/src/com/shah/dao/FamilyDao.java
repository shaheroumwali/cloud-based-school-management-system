package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Family;
import com.shah.pojo.Parent;
public class FamilyDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root","shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
public static int save(Family e) {
	int status=0;
	try {
		Connection con = FamilyDao.getConnection();
	
		PreparedStatement ps = con.prepareStatement("insert into famliies("
				+ "name,"
				+ "created_at,"
				+ "modified_at,"
				+ "created_by,"
				+ "modified_by,"
				+ "head_of_family_parents_id"
				+ ") values (?,?,?,?,?,?)");
		ps.setString(1, e.getName());
		
		Date cr_dt = new Date();
		java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
		ps.setTimestamp(2, cr_dt_time);
		ps.setTimestamp(3, cr_dt_time);
		ps.setInt(4, e.getCreatedBy());
		ps.setInt(5, e.getModifiedBy());
		ps.setInt(6, e.getParent().getId());
		status = ps.executeUpdate();
	}
	catch (Exception e2) {
		System.out.println(e2 + " error in database save method");
	}
	return status;
	
}
public static int update(Family e){  
    int status=0;  
    try{  
        Connection con=FamilyDao.getConnection();  
        PreparedStatement ps=con.prepareStatement(  
        "update famliies set name=?,head_of_family_parents_id=? where id=?");  
        ps.setString(1,e.getName());
        ps.setInt(2,e.getHead_of_family_parents_id());
        ps.setInt(3,e.getId());
		status = ps.executeUpdate();
        con.close();  
    }catch(Exception ex)
    {ex.printStackTrace();}  
    return status;  
}

public static int delete(int id){  
    int status=0;
    try{  
        Connection con=FamilyDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("delete from famliies where id=?");  
        ps.setInt(1,id);  
        status=ps.executeUpdate();  
          
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return status;  
}
public static Family getFamilyById(int id){  
    Family e=new Family();      
    try{  
        Connection con=FamilyDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from famliies where id=?");  
        ps.setInt(1,id);  
        ResultSet rs=ps.executeQuery();  
        if(rs.next()){  
        	e.setId(rs.getInt(1));
        	e.setName(rs.getString(2));
        	e.setHead_of_family_parents_id(rs.getInt(7));
        	
        	    
	                 }  
        con.close();  
    }catch(Exception ex){ex.printStackTrace();}  
      
    return e;  
}
public static List<Family> getAllFamily(){  
    List<Family> list=new ArrayList<Family>();  
      
    try{  
        Connection con=FamilyDao.getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from famliies");  
        ResultSet rs=ps.executeQuery();  
        while(rs.next()){  
            Family e=new Family();  
      	  e.setId(rs.getInt(1));
      	  e.setName(rs.getString(2));
         list.add(e);  
        }  
        con.close();  
    }catch(Exception e){e.printStackTrace();}  
      
    return list;  
}
public static List<Family> getAllFamilyWithParentNames() {
	List<Family> list = new ArrayList<Family>();

	try {
		Connection con = FamilyDao.getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT f.*,p.first_name,p.last_name,p.gender FROM school_management_system.famliies f LEFT OUTER JOIN parents p ON p.id=f.head_of_family_parents_id;");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Family e = new Family();
			 e.setId(rs.getInt(1));
            e.setName(rs.getString(2));
            
            Parent p = new Parent();
            p.setId(rs.getInt(7));
            p.setFirstname(rs.getString(9));
            p.setLastname(rs.getString(10));
            p.setGender(rs.getString(11));
            e.setParent(p);
            		list.add(e);
		}
		con.close();
	} catch (Exception e) {
		System.out.println("Error In getAllFamilyWithParentNames ");
	}

	return list;
}
}

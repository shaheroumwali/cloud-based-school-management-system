package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.shah.pojo.Student;
import com.shah.pojo.StudentClasses;

public class StudentClassDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}

	public static int save(StudentClasses e) {
		int status = 0;
		try {
			Connection con = StudentClassDao.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into student_classes("
			+ "date_from,"
			+ "date_to,"	
			+ "created_at,"
			+ "modified_at,"
			+ "created_by,"
			+ "modified_by,"
			+ "classes_id,"
			+ "students_id"
					+ ") values (?,?,?,?,?,?,?,?)");
			java.util.Date dF = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressFrom());
			java.sql.Date df = new java.sql.Date(dF.getTime());
			ps.setDate(1, df);
			java.util.Date dT = new SimpleDateFormat("YYYY-MM-DD").parse(e.getDateAddressTo());
			java.sql.Date dt = new java.sql.Date(dT.getTime());
			ps.setDate(2, dt);
			Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(3, cr_dt_time);
			ps.setTimestamp(4, cr_dt_time);
			ps.setInt(5, 2);
			ps.setInt(6, 2);
			System.out.println(e.getClasses().getId());
			ps.setInt(7, e.getClasses().getId());
			ps.setInt(8, e.getStudent().getId());
			status = ps.executeUpdate();
			
		} catch (Exception e2) {
			System.out.println(e2 + " error in database save method");
		}
		return status;
	}
	public static StudentClasses getStudentClasById(int id) {
		StudentClasses e = new StudentClasses();
		try {
			Connection con = StudentClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from student_classes where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setDateAddressFrom(rs.getString(2));
				e.setDateAddressTo(rs.getString(4));
				e.setClass_id(rs.getInt(8));
				e.setStudent_id(rs.getInt(9));
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}

	public static int update(StudentClasses e) {
		int status = 0;
		try {
			Connection con = StudentClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update student_classes set date_from=?,date_to=?,classes_id=?,students_id=? where id=?");
			ps.setString(1, e.getDateAddressFrom());
			ps.setString(2, e.getDateAddressTo());
			//ps.setString(3, e.getAddressDetail());
			ps.setInt(3, e.getClass_id());
			ps.setInt(4, e.getStudent_id());
			ps.setInt(5, e.getId());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = StudentClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from student_classes where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static List<StudentClasses> getAllStudentClassWithClassandStudentNames() {
		List<StudentClasses> list = new ArrayList<StudentClasses>();

		try {
			Connection con = StudentClassDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT sc.*,cls.code,cls.name,st.first_name,st.last_name FROM school_management_system.student_classes sc LEFT OUTER JOIN classes cls ON cls.id=sc.classes_id LEFT OUTER JOIN students st ON sc.students_id=st.id;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				StudentClasses e = new StudentClasses();
				e.setId(rs.getInt(1));
				e.setDateAddressFrom(rs.getString(2));
				e.setDateAddressTo(rs.getString(4));

				com.shah.pojo.Class a = new com.shah.pojo.Class();
				a.setId(rs.getInt(8));
				a.setCode(rs.getString(11));
				a.setName(rs.getString(12));
				e.setClasses(a);

				Student s = new Student();
				s.setId(rs.getInt(9));
				s.setfirstname(rs.getString(12));
				s.setlastname(rs.getString(13));
				e.setStudent(s);
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Error In getAllStudentClassWithClassAndStudentNames() ");
		}

		return list;
	}

}

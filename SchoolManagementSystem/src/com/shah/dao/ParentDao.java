package com.shah.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shah.pojo.Parent;
public class ParentDao {
	public static Connection getConnection() {

		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school_management_system", "root",
					"shaheroum1997");
		} catch (Exception ex) {
			System.out.println("database error" + ex);
		}
		return con;
	}
	public static int save(Parent e) {
		int status = 0;
		try {
			Connection con = ParentDao.getConnection();

			PreparedStatement ps = con.prepareStatement(
					"insert into parents("
					+ "first_name,"
					+ "last_name,"
					+ "gender,"
					+ "other_parent_details,"
					+ "created_at,"
					+ "modified_at,"
					+ "created_by,"
					+ "modified_by"
					+ ") values "
					+ "(?,?,?,?,?,?,?,?)");
			ps.setString(1, e.getFirstname());
			ps.setString(2, e.getLastname());
			ps.setString(3, e.getGender());
			ps.setString(4, e.getOPD());
						Date cr_dt = new Date();
			java.sql.Timestamp cr_dt_time = new java.sql.Timestamp(cr_dt.getTime());
			ps.setTimestamp(5, cr_dt_time);
			ps.setTimestamp(6, cr_dt_time);
			ps.setInt(7,1);
			ps.setInt(8,2);

			status = ps.executeUpdate();
		} catch (Exception e2) {

			System.out.println(e2 + " error in database save methode");
		}
		return status;

	}
	public static int update(Parent e) {
		int status = 0;
		try {
			Connection con = ParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update parents set first_name=?,last_name=?,gender=?,other_parent_details=? where id=?");
			
			ps.setString(1, e.getFirstname());
			ps.setString(2, e.getLastname());
			ps.setString(3, e.getGender());
			ps.setString(4, e.getOPD());
			ps.setInt(5, e.getId());
			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;

	}

	public static int delete(int id) {
		int status = 0;
		try {
			Connection con = ParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from parents where id=?");
			ps.setInt(1, id);
			status = ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static Parent getParentById(int id) {
		Parent e = new Parent();

		try {
			Connection con = ParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from parents where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				e.setId(rs.getInt(1));
				e.setFirstname(rs.getString(2));
				e.setLastname(rs.getString(3));
				e.setGender(rs.getString(4));
				e.setOPD(rs.getString(5));
				//e.setId(id);
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return e;
	}
	public static List<Parent> getAllParents() {
		List<Parent> list = new ArrayList<Parent>();

		try {
			Connection con = ParentDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from parents");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Parent e = new Parent();
				e.setId(rs.getInt(1));
				e.setFirstname(rs.getString(2));
				e.setLastname(rs.getString(3));
				e.setGender(rs.getString(4));
				e.setOPD(rs.getString(5));
				list.add(e);
			}
			con.close();
		} catch (Exception e) {
			System.out.println("error in All Parent method");
		}

		return list;
	}
	

}

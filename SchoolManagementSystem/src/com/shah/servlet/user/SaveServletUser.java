package com.shah.servlet.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserDao;
import com.shah.pojo.User;

/**
 * Servlet implementation class SaveServletUser
 */
@WebServlet("/SaveServletUser")
public class SaveServletUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveServletUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();

		String Uname = request.getParameter("username");

		String email = request.getParameter("email");

		if (UserDao.isUsernameAvailable(Uname) && UserDao.isEmailAvailable(email)) {

			String pword = request.getParameter("pwd");
			String fname = request.getParameter("firstname");
			String lname = request.getParameter("lastname");
			String gen = request.getParameter("gender");
			String dateofbirth = request.getParameter("dateofbirth");
			String pNumber = request.getParameter("phonenumber");
			// int schoolId = Integer.parseInt(request.getParameter("schoolID"));

			User e = new User();
			e.setUsername(Uname);
			e.setPassword(pword);
			e.setFirstname(fname);
			e.setLastname(lname);
			e.setGender(gen);
			e.setEmail(email);
			e.setBirthdate(dateofbirth);
			e.setPhoneNumber(pNumber);
			// e.setSchoolId(schoolId);

			int status = UserDao.save(e);
			if (status > 0) {
				
				request.setAttribute("successMsg", "Record saved successfully!");
				request.getRequestDispatcher("User.jsp").include(request, response);
			} else {
				request.setAttribute("errorMsg", "Sorry! unable to save record");
			}
		}else {
			pw.println("Username/email is already in database...");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 *      protected void doPost(HttpServletRequest request, HttpServletResponse
	 *      response) throws ServletException, IOException { // TODO Auto-generated
	 *      method stub doGet(request, response); }
	 */
}

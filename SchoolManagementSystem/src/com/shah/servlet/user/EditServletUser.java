package com.shah.servlet.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserDao;
import com.shah.pojo.User;

/**
 * Servlet implementation class EditServletUser
 */
@WebServlet("/EditServletUser")
public class EditServletUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServletUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	*/ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
	 	PrintWriter pw = response.getWriter();
	 	 String sid=request.getParameter("id");  
		    int id=Integer.parseInt(sid); 
		    String Uname = request.getParameter("username");
			String pword = request.getParameter("pswd");
			String fname = request.getParameter("firstname");
			String  lname = request.getParameter("lastname");
			String gen = request.getParameter("gender");
			String email = request.getParameter("email");
			String dateofbirth =request.getParameter("dateofbirth");
			String pNumber = request.getParameter("phonenumber");
			    User e = new User();
				e.setId(id);
			    e.setUsername(Uname);
			    e.setPassword(pword);
			    e.setFirstname(fname);
			    e.setLastname(lname);
			    e.setGender(gen);
			    e.setEmail(email);
			    e.setBirthdate(dateofbirth);
			    e.setPhoneNumber(pNumber);
		int status=UserDao.update(e);
		
        if(status>0){  
            response.sendRedirect("ViewServletUser");  
        }else{  
            pw.println("Sorry! unable to update record");  
        }
		
	}
	
	}



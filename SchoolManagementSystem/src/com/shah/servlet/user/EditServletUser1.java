package com.shah.servlet.user;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserDao;
import com.shah.pojo.User;


/**
 * Servlet implementation class EditServletUser1
 */
@WebServlet("/EditServletUser1")
public class EditServletUser1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServletUser1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	   String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);  
	      
	    User userList =UserDao.getUserById(id);  
	  
	    request.setAttribute("userList", userList);

        RequestDispatcher rd = request.getRequestDispatcher("User_Edit.jsp");
        rd.include(request, response);
	      
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

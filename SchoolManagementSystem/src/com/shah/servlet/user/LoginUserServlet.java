package com.shah.servlet.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.UserDao;
import com.shah.pojo.User;

/**
 * Servlet implementation class LoginUserServlet
 */
@WebServlet("/LoginUserServlet")
public class LoginUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		
		String uname = request.getParameter("uname");
		String psw = request.getParameter("psw");
		User CurrentUser= UserDao.getUserByUnameNPassword(uname, psw, "d");
		if(CurrentUser.getId() >= 1) {
	        HttpSession session=request.getSession();        
	        session.setAttribute("CUSER", CurrentUser);
			out.println("Hello. " + CurrentUser.getFirstname() + " " + CurrentUser.getLastname());
		} else {
			out.println("Sorry, Username/password does not match.. ");
		}	
		out.close();
		
	}
	
	 

}

package com.shah.servlet.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserDao;

/**
 * Servlet implementation class DeleteServletUser
 */
@WebServlet("/DeleteServletUser")
public class DeleteServletUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
0     * @see HttpServlet#HttpServlet()
     */
    public DeleteServletUser() {
        super();
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String sid =request.getParameter("id");
		int id=Integer.parseInt(sid);
		UserDao.delete(id);
		response.sendRedirect("ViewServletUser");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

package com.shah.servlet.ParentAddresses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ParentAddressDao;
import com.shah.pojo.ParentAddress;

/**
 * Servlet implementation class ParentAddressSaveEditServlet
 */
@WebServlet("/ParentAddressSaveEditServlet")
public class ParentAddressSaveEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ParentAddressSaveEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 *      protected void doGet(HttpServletRequest request, HttpServletResponse
	 *      response) throws ServletException, IOException { // TODO Auto-generated
	 *      method stub
	 *      response.getWriter().append("Served at: ").append(request.getContextPath());
	 *      }
	 * 
	 */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter pw = response.getWriter();

		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);
		String dateFrom = request.getParameter("dateaddrfrom");
		String dateTo = request.getParameter("dateaddrto");
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
		int address_id = Integer.parseInt(request.getParameter("address_id"));

		ParentAddress e = new ParentAddress();
		e.setId(id);
		e.setDateAddress_from(dateFrom);
		e.setDateddressT(dateTo);
		e.setParent_id(parent_id);
		e.setAddresses_id(address_id);
		

		int status = ParentAddressDao.update(e);

		if (status > 0) {
			response.sendRedirect("ParentAddressViewServlet");
		} else {
			pw.println("Sorry! unable to update record");
		}

	}

}

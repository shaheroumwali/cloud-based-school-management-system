package com.shah.servlet.ParentAddresses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ParentAddressDao;
import com.shah.dao.ParentDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.Parent;
import com.shah.pojo.ParentAddress;

/**
 * Servlet implementation class ParentAddressEditServlet
 */
@WebServlet("/ParentAddressEditServlet")
public class ParentAddressEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ParentAddressEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);    
	    
	    ParentAddress parentAddressList = ParentAddressDao.getParentAddressById(id);
	    request.setAttribute("parentAddressList", parentAddressList);
	    
	    ArrayList<Parent> parentList = (ArrayList<Parent>) ParentDao.getAllParents();
	    request.setAttribute("parentList", parentList);	
	    
	    ArrayList<Addresses> addressesList=(ArrayList<Addresses>) addressesDao.getAllAddresses();  
	    request.setAttribute("addressesList", addressesList);
	    
	    RequestDispatcher rd =request.getRequestDispatcher("ParentAddresses_Edit.jsp");
	    rd.include(request, response);

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
*/	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

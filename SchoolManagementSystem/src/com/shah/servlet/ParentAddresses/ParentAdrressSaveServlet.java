package com.shah.servlet.ParentAddresses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ParentAddressDao;
import com.shah.dao.ParentDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.Parent;
import com.shah.pojo.ParentAddress;
import com.shah.pojo.User;

/**
 * Servlet implementation class ParentAdrressSaveServlet
 */
@WebServlet("/ParentAdrressSaveServlet")
public class ParentAdrressSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ParentAdrressSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		response.setContentType("text/html");
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		
	 	
	 	    String dateFrom = request.getParameter("DAF");
			String dateTo  = request.getParameter("DAT");
			int parent_id = Integer.parseInt(request.getParameter("parent_id"));
			int address_id = Integer.parseInt(request.getParameter("address_id"));
			
			Parent p = ParentDao.getParentById(parent_id);
			Addresses ad = addressesDao.getAddressById(address_id);
			
			
				    ParentAddress e = new ParentAddress();
			    e.setDateAddress_from(dateFrom);
			    e.setDateddressT(dateTo);
			    e.setAddresses(ad);
			    e.setParent(p);
			    
		           int status=ParentAddressDao.save(e);
        if(status>0){  
        	request.setAttribute("successMsg", "Record saved successfully!");
        	request.getRequestDispatcher("AddParentAddressServlet").include(request, response);  
        }else{  
        	request.setAttribute("errorMsg", "Sorry! unable to save record");  
        }

		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

	}

	}
	}

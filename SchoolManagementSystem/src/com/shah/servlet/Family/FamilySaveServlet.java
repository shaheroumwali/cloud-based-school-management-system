package com.shah.servlet.Family;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.FamilyDao;
import com.shah.dao.ParentDao;
import com.shah.pojo.Family;
import com.shah.pojo.Parent;
import com.shah.pojo.User;

/**
 * Servlet implementation class FamilySaveServlet
 */
@WebServlet("/FamilySaveServlet")
public class FamilySaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilySaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
	      HttpSession session = request.getSession();
	      if(session.getAttribute("CUSER") != null ){
	    	  User u = (User)session.getAttribute("CUSER");
	    	  if(u.getId()>= 1) {
	    	  
	      }
	
		String name = request.getParameter("Familyname");
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
		Parent p = ParentDao.getParentById(parent_id);
		Family e = new Family();
		e.setName(name);
		e.setParent(p);
		int status = FamilyDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddFamilyServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
	}
	
	else {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("Not authorized...");
}
		
	}

}

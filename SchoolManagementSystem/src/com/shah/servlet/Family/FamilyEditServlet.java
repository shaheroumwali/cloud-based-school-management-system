package com.shah.servlet.Family;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.FamilyDao;
import com.shah.dao.ParentDao;
import com.shah.pojo.Family;
import com.shah.pojo.Parent;


/**
 * Servlet implementation class FamilyEditServlet
 */
@WebServlet("/FamilyEditServlet")
public class FamilyEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);    
	    
	    Family familyList=FamilyDao.getFamilyById(id);  
	    request.setAttribute("familyList", familyList);
	    
	    ArrayList<Parent> parentList = (ArrayList<Parent>) ParentDao.getAllParents();
	    request.setAttribute("parentList", parentList);
	    
	    RequestDispatcher rd =request.getRequestDispatcher("Family_Edit.jsp");
	    rd.include(request, response);
	 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

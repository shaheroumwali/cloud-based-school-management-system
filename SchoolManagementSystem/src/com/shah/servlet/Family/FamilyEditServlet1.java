package com.shah.servlet.Family;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.FamilyDao;
import com.shah.pojo.Family;
/**
 * Servlet implementation class FamilyEditServlet1
 */
@WebServlet("/FamilyEditServlet1")
public class FamilyEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
		String name = request.getParameter("Fname");
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
		
		Family e = new Family();
		e.setId(id);
		e.setName(name);
		e.setHead_of_family_parents_id(parent_id);
		
		int status = FamilyDao.update(e);
		if (status > 0) {
			  response.sendRedirect("FamilyViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}

	}

}

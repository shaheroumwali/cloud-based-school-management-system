package com.shah.servlet.payement;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementTypeDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.PayementType;
import com.shah.pojo.Student;

/**
 * Servlet implementation class AddPayementServlet
 */
@WebServlet("/AddPayementServlet")
public class AddPayementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPayementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		ArrayList<PayementType> payementTypeList  =(ArrayList<PayementType>) PayementTypeDao.getAllPayementType();
		request.setAttribute("payementTypeList", payementTypeList);
		
		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();
		request.setAttribute("studentList" , studentList);
		RequestDispatcher rd = request.getRequestDispatcher("Payements.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

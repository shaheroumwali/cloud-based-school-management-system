package com.shah.servlet.payement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementDao;
import com.shah.pojo.Payements;

/**
 * Servlet implementation class PayementEditServlet1
 */
@WebServlet("/PayementEditServlet1")
public class PayementEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
		    int id=Integer.parseInt(request.getParameter("id")); 
			double  amount = Double.parseDouble(request.getParameter("amount"));
			String pdate = request.getParameter("pDate");
			int ptID = Integer.parseInt(request.getParameter("ptId"));
			
			
			int payementType_id = Integer.parseInt(request.getParameter("payementType_id"));
			int student_id = Integer.parseInt(request.getParameter("student_id"));

			Payements e = new Payements();
			e.setId(id);
			e.setAmount(amount);
			e.setPayDate(pdate);
			e.setPayTypeId(ptID);
			e.setPayementType_id(payementType_id);
			e.setStudent_id(student_id);

			int status = PayementDao.update(e);
			if (status > 0) {
				response.sendRedirect("ViewPayementServlet");
			} else {
				pw.println("Sorry! unable to update record");

	}
		}
		}


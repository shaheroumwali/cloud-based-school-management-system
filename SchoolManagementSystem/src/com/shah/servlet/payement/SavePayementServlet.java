package com.shah.servlet.payement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.PayementDao;
import com.shah.dao.PayementTypeDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.PayementType;
import com.shah.pojo.Payements;
import com.shah.pojo.Student;
import com.shah.pojo.User;

/**
 * Servlet implementation class SavePayementServlet
 */
@WebServlet("/SavePayementServlet")
public class SavePayementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SavePayementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
       //doGet(request, response);
		response.setContentType("text/html");

		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
	
		String Amount = request.getParameter("amount");
		double amount = Double.parseDouble(Amount);
		String pdate = request.getParameter("pDate");
		String payTypeId = request.getParameter("ptId");
		int ptid = Integer.parseInt(payTypeId);
		
		int payementType_id = Integer.parseInt(request.getParameter("payementType_id"));
		int student_id = Integer.parseInt(request.getParameter("student_id"));
		
		PayementType pT = PayementTypeDao.getPayemenTypeById(payementType_id);
		Student s = StudentDao.getStudentById(student_id);
		
		
		Payements e = new Payements();
		e.setAmount(amount);
		e.setPayDate(pdate);;
		e.setPayTypeId(ptid);
		e.setPayementType(pT);
		e.setStudent(s);
		int status = PayementDao.save(e);
		if (status > 0) {
		  	request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddPayementServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
	}
else {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	out.print("Not authorized...");
}

}
}
}
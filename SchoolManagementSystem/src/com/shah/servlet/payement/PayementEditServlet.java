package com.shah.servlet.payement;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementDao;
import com.shah.dao.PayementTypeDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.PayementType;
import com.shah.pojo.Payements;
import com.shah.pojo.Student;


/**
 * Servlet implementation class PayementEditServlet
 */
@WebServlet("/PayementEditServlet")
public class PayementEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);  
	    
	    Payements payementList=PayementDao.getPayementById(id);
	    request.setAttribute("payementList", payementList);
	    
	    ArrayList<PayementType>  payementTypeList = (ArrayList<PayementType>) PayementTypeDao.getAllPayementType();
	    request.setAttribute("payementTypeList", payementTypeList);
	    
	    ArrayList<Student> studentList =(ArrayList<Student>) StudentDao.getAllStudents(); 
	    request.setAttribute("studentList", studentList );
	    
	    
	    RequestDispatcher rd = request.getRequestDispatcher("Payement_Edit.jsp");
	    rd.include(request, response);
	     
	   
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

package com.shah.servlet.school;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.SchoolDao;
import com.shah.pojo.School;
import com.shah.pojo.User;
/**
 * Servlet implementation class SchoolSaveServlet
 */
@WebServlet("/SchoolSaveServlet")
public class SchoolSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SchoolSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { // TODO Auto-generated
	 * method stub response.getWriter().append("Served at:
	 * ").append(request.getContextPath()); }
	 * 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {

		
				response.setContentType("text/html");
	
				String sname = request.getParameter("sname");
				String pname = request.getParameter("schoolprincipal");
				String osd = request.getParameter("otherSchoolDetail");

				School e = new School();
				e.setName(sname);
				e.setPrincipalName(pname);
				e.setOther_school_details(osd);
				
				int status = SchoolDao.save(e);
				if (status > 0) {
					request.setAttribute("successMsg", "Record saved successfully!");
					request.getRequestDispatcher("school.jsp").include(request, response);
				} else {
					request.setAttribute("errorMsg", "Sorry! unable to save record");
				}
			}
			
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

	}


}

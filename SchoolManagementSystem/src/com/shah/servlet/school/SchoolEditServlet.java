package com.shah.servlet.school;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolDao;

import com.shah.pojo.School;

/**
 * Servlet implementation class SchoolEditServlet
 */
@WebServlet("/SchoolEditServlet")
public class SchoolEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SchoolEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);       
	    
	    School schoolList=SchoolDao.getSchoolById(id); 
	    request.setAttribute("schoolList", schoolList);
	    
	    RequestDispatcher rd = request.getRequestDispatcher("School_Edit.jsp");
	    		rd.include(request, response);
	    
	     
	 

	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}


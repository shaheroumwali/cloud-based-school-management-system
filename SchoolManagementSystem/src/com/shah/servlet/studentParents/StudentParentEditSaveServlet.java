package com.shah.servlet.studentParents;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.shah.dao.StudentParentDao;

import com.shah.pojo.StudentParent;

/**
 * Servlet implementation class StudentParentEditSaveServlet
 */
@WebServlet("/StudentParentEditSaveServlet")
public class StudentParentEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentParentEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
	
	    int student_id = Integer.parseInt(request.getParameter("student_id"));
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
 
		StudentParent e = new StudentParent();
		e.setId(id);
		e.setStudent_id(student_id);
		e.setParent_id(parent_id);
		
		int status = StudentParentDao.update(e);
	
	
		if (status > 0) {
			  response.sendRedirect("StudentParentViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}

	
	}

}

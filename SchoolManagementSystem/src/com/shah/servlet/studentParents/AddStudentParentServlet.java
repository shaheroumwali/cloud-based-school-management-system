package com.shah.servlet.studentParents;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ParentDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Parent;
import com.shah.pojo.Student;

/**
 * Servlet implementation class AddStudentAddressServlet
 */
@WebServlet("/AddStudentParentServlet")
public class AddStudentParentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddStudentParentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();
		request.setAttribute("studentList", studentList);
		
		ArrayList<Parent> parentList = (ArrayList<Parent>) ParentDao.getAllParents();
		request.setAttribute("parentList", parentList);
		
		
		
		RequestDispatcher rd = request.getRequestDispatcher("StudentParent.jsp");
		rd.include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.shah.servlet.FamilyMember;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.FamilyDao;
import com.shah.dao.FamilyMemberDao;
import com.shah.dao.ParentDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Family;
import com.shah.pojo.FamilyMember;
import com.shah.pojo.Parent;
import com.shah.pojo.Student;
import com.shah.pojo.User;

/**
 * Servlet implementation class FamilyMemberSaveServlet
 */
@WebServlet("/FamilyMemberSaveServlet")
public class FamilyMemberSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyMemberSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
			
		}
		String name = request.getParameter("FMname");
		int family_id = Integer.parseInt(request.getParameter("family_id"));
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
		int student_id = Integer.parseInt(request.getParameter("student_id"));
		Family f = FamilyDao.getFamilyById(family_id);
		Parent p = ParentDao.getParentById(parent_id);
		Student s = StudentDao.getStudentById(student_id);
		
		FamilyMember e = new FamilyMember();
		//e.setId(id);
		e.setName(name);
		e.setFamily(f);
		e.setParent(p);
		e.setStudent(s);
		int status = FamilyMemberDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddFamilyMemberServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		
	}
		else {
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			pw.print("Not authorized...");
			}
		}

	}



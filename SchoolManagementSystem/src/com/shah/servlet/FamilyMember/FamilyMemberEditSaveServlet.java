package com.shah.servlet.FamilyMember;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.FamilyMemberDao;
import com.shah.pojo.FamilyMember;


/**
 * Servlet implementation class FamilyMemberEditSaveServlet
 */
@WebServlet("/FamilyMemberEditSaveServlet")
public class FamilyMemberEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyMemberEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
	
		String name = request.getParameter("Fname");
		int family_id = Integer.parseInt(request.getParameter("family_id"));
		int parent_id = Integer.parseInt(request.getParameter("parent_id"));
		int student_id = Integer.parseInt(request.getParameter("student_id"));
		
		
		FamilyMember e = new FamilyMember();
		e.setId(id);
		e.setName(name);
		e.setFamily_id(family_id);
		e.setParent_id(parent_id);
		e.setStudent_id(student_id);
		int status = FamilyMemberDao.update(e);
	
	
		if (status > 0) {
			  response.sendRedirect("FamilyMemberViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}

	}

	
	}



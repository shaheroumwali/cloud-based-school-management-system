package com.shah.servlet.FamilyMember;

import java.io.IOException;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.FamilyMemberDao;
import com.shah.pojo.FamilyMember;

/**
 * Servlet implementation class FamilyMemberViewServlet
 */
@WebServlet("/FamilyMemberViewServlet")
public class FamilyMemberViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyMemberViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		List<FamilyMember> list=FamilyMemberDao.getAllFamilyMemberWithFamilyParentandStudentsName();  
		 request.setAttribute("familyMemberList", list);
		 
		RequestDispatcher rd = request.getRequestDispatcher("FamilyMember_View.jsp");
		rd.include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

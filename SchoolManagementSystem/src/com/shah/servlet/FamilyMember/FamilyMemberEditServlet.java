package com.shah.servlet.FamilyMember;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.FamilyDao;
import com.shah.dao.FamilyMemberDao;
import com.shah.dao.ParentDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Family;
import com.shah.pojo.FamilyMember;
import com.shah.pojo.Parent;
import com.shah.pojo.Student;

/**
 * Servlet implementation class FamilyMemberEditServlet
 */
@WebServlet("/FamilyMemberEditServlet")
public class FamilyMemberEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyMemberEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);    
	    
	    FamilyMember familyMemberList = FamilyMemberDao.getFamilyMemberById(id);
	    request.setAttribute("familyMemberList", familyMemberList);
	    
	   ArrayList<Family> familyList=(ArrayList<Family>) FamilyDao.getAllFamily();  
	    request.setAttribute("familyList", familyList);
	    
	    ArrayList<Parent> parentList = (ArrayList<Parent>) ParentDao.getAllParents();
	    request.setAttribute("parentList", parentList);
	    
	    ArrayList<Student> studentList = (ArrayList<Student>)StudentDao.getAllStudents();
	    request.setAttribute("studentList", studentList);
	    
	    
	    RequestDispatcher rd =request.getRequestDispatcher("FamilyMember_Edit.jsp");
	    rd.include(request, response);

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

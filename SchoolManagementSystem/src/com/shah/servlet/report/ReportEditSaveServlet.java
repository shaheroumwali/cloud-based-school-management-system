package com.shah.servlet.report;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.ReportDao;
import com.shah.pojo.Report;

/**
 * Servlet implementation class ReportEditSaveServlet
 */
@WebServlet("/ReportEditSaveServlet")
public class ReportEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
		String dateCreated = request.getParameter("DC");
		String reportContent = request.getParameter("RepContent");
		String TComment = request.getParameter("TC");
		String otherReportwork = request.getParameter("ORD");
		int student_id = Integer.parseInt(request.getParameter("student_id"));

		
		
		Report e = new Report();
		e.setId(id);
		e.setDateCreated(dateCreated);
		e.setReportContent(reportContent);
		e.setTeacherComment(TComment);
		e.setOtherReportDetail(otherReportwork);
		e.setStudent_id(student_id);
		
		int status = ReportDao.update(e);
	
		if (status > 0) {
			  response.sendRedirect("ReportViewServlet");
		} else {
			pw.println("Sorry! unable to update record");
}
	
	}

}

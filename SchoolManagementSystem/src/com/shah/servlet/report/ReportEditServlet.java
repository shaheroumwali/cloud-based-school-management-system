package com.shah.servlet.report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ReportDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Report;
import com.shah.pojo.Student;

/**
 * Servlet implementation class ReportEditServlet
 */
@WebServlet("/ReportEditServlet")
public class ReportEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);
	    
	    Report reportList = ReportDao.getReportById(id);
	    request.setAttribute("reportList", reportList);
	    
	    ArrayList<Student> studentList =(ArrayList<Student>) StudentDao.getAllStudents(); 
	    request.setAttribute("studentList", studentList);
	    
	    RequestDispatcher rd =request.getRequestDispatcher("Report_Edit.jsp");
	    rd.include(request, response);
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

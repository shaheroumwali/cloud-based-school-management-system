package com.shah.servlet.report;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ReportDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Report;
import com.shah.pojo.Student;
import com.shah.pojo.User;

/**
 * Servlet implementation class ReportSaveServlet
 */
@WebServlet("/ReportSaveServlet")
public class ReportSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { // TODO Auto-generated
	 * method stub response.getWriter().append("Served at:
	 * ").append(request.getContextPath()); }
	 * 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		String dateCreated = request.getParameter("DC");
		String reportContent = request.getParameter("RepContent");
		String TComment = request.getParameter("TC");
		String otherReportwork = request.getParameter("ORD");
		int student_id = Integer.parseInt(request.getParameter("student_id"));

		Student p = StudentDao.getStudentById(student_id);
		
		Report e = new Report();
		e.setDateCreated(dateCreated);
		e.setReportContent(reportContent);
		e.setTeacherComment(TComment);
		e.setOtherReportDetail(otherReportwork);
		e.setStudent(p);
		
		int status = ReportDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddReportServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
	
	}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

}
}
package com.shah.servlet.userType;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.UserTypeDao;
import com.shah.pojo.User;
import com.shah.pojo.UserType;

/**
 * Servlet implementation class UserTypeSaveServlet
 */
@WebServlet("/UserTypeSaveServlet")
public class UserTypeSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserTypeSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		String userT = request.getParameter("UNT");
		String description = request.getParameter("description");
		UserType e =new UserType();
		e.setUserType(userT);
		e.setDescription(description);
		int status = UserTypeDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("UserType.jsp").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		out .close();
		
	}
		} else {
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			pw.print("Not authorized...");
	}

}
}
package com.shah.servlet.userType;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.shah.dao.UserTypeDao;
import com.shah.pojo.UserType;

/**
 * Servlet implementation class UserTypeEditServlet1
 */
@WebServlet("/UserTypeEditServlet1")
public class UserTypeEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserTypeEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		response.setContentType("text/html");
	 	PrintWriter pw = response.getWriter();
	 	 String sid=request.getParameter("id");  
		    int id=Integer.parseInt(sid); 
		String Usertype = request.getParameter("UserType");	
		String description = request.getParameter("description"); 
		
		    UserType e = new UserType();
		e.setId(id);
	    e.setUserType(Usertype);
	    e.setDescription(description);
		int status=UserTypeDao.update(e);
        if(status>0){  
            response.sendRedirect("UserTypeViewServlet");  
        }else{  
            pw.println("Sorry! unable to update record");  
        }
	}

}

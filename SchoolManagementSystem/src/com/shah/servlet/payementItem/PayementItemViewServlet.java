package com.shah.servlet.payementItem;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementItemDao;
import com.shah.pojo.PayementItem;
/**
 * Servlet implementation class PayementItemViewServlet
 */
@WebServlet("/PayementItemViewServlet")
public class PayementItemViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementItemViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   
        List<PayementItem> list=PayementItemDao.getAllPayementItemWithPayements(); 
        request.setAttribute("payementItemList", list);
        
		RequestDispatcher rd = request.getRequestDispatcher("PayementItem_view.jsp");
		rd.include(request, response);     
        

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)iTEM
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}


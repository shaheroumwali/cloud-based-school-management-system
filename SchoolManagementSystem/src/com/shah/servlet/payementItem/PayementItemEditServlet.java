package com.shah.servlet.payementItem;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementDao;
import com.shah.dao.PayementItemDao;
import com.shah.pojo.PayementItem;
import com.shah.pojo.Payements;
/**
 * Servlet implementation class PayementItemEditServlet
 */
@WebServlet("/PayementItemEditServlet")
public class PayementItemEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementItemEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);       
	    
	    
	    PayementItem payementItemList=PayementItemDao.getPayementItemById(id);
	    request.setAttribute("payementItemList", payementItemList);
	    
	    ArrayList<Payements> payementsList = (ArrayList<Payements>)PayementDao.getAllPayements();
	    request.setAttribute("payementList", payementsList);
	    
	    
	    RequestDispatcher rd = request.getRequestDispatcher("PayementItem_Edit.jsp");
	    rd.include(request, response);
	    
	    
	     
	 

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

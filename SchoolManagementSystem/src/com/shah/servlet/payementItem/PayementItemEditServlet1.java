package com.shah.servlet.payementItem;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementItemDao;
import com.shah.pojo.PayementItem;
/**
 * Servlet implementation class PayementItemEditServlet1
 */
@WebServlet("/PayementItemEditServlet1")
public class PayementItemEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementItemEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid); 
	    String Description = request.getParameter("description");
		String Ammount = request.getParameter("amount");
		double  amount = Double.parseDouble(Ammount);
		int payement_id = Integer.parseInt(request.getParameter("payement_id"));
		
		PayementItem e = new PayementItem();
		e.setId(id);
		e.setDescription(Description);
		e.setAmount(amount);
		e.setPayement_id(payement_id);
		
		int status = PayementItemDao.update(e);
		if (status > 0) {
			
			response.sendRedirect("PayementItemViewServlet");
			//request.getRequestDispatcher("PayementItemViewServlet").include(request, response);
		} else {
			pw.println("Sorry! unable to update record");

}

	}

}

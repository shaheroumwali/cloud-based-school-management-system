package com.shah.servlet.payementItem;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.PayementDao;
import com.shah.dao.PayementItemDao;
import com.shah.pojo.PayementItem;
import com.shah.pojo.Payements;
import com.shah.pojo.User;

/**
 * Servlet implementation class PayementItemSaveServlet
 */
@WebServlet("/PayementItemSaveServlet")
public class PayementItemSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementItemSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");

		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		
		String description = request.getParameter("description");
		String Amount = request.getParameter("amount");
	
		double amount = Double.parseDouble(Amount);
		int Payement_id = Integer.parseInt(request.getParameter("payement_id"));
		Payements payement = PayementDao.getPayementById(Payement_id);
		
		
		PayementItem e = new PayementItem();
		e.setAmount(amount);
		e.setDescription(description);
		e.setPayement(payement);
		int status = PayementItemDao.save(e);
		if (status > 0) {
		
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddPayementItemServlet").include(request, response);
		} else {
		
		}	request.setAttribute("errorMsg", "Sorry! unable to save record");

	}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

		}

}

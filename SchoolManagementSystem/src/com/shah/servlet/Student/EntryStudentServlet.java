package com.shah.servlet.Student;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.SchoolDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Class;
import com.shah.pojo.School;
import com.shah.pojo.Student;

/**
 * Servlet implementation class EntryStudentServlet
 */
@WebServlet("/EntryStudentServlet")
public class EntryStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryStudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
		request.setAttribute("schoolList", schoolList);

		ArrayList<Class> ClassList = (ArrayList<Class>) ClassDao.getAllClasses();
		request.setAttribute("ClassList", ClassList) ;
		
		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();				
		request.setAttribute("studentList" , studentList);
				   
		RequestDispatcher rd = request.getRequestDispatcher("add_student.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

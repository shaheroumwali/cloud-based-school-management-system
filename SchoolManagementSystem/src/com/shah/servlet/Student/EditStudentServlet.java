package com.shah.servlet.Student;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Class;
import com.shah.pojo.Student;
import com.shah.pojo.StudentAddress;
import com.shah.pojo.StudentClasses;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/EditServlet")
public class EditStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditStudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		response.setContentType("text/html");  
	 	PrintWriter pw = response.getWriter();
	 	 String sid=request.getParameter("id");  
		    int student_id=Integer.parseInt(sid); 
		String rn = request.getParameter("rollno");
		int rollno = Integer.parseInt(rn);
		String gen = request.getParameter("gender");
		String fname = request.getParameter("firstname");
		String  lname = request.getParameter("lastname");
		String  dob = request.getParameter("dateofbirth");
		String tfee =  request.getParameter("tuitionfee");
		double tf = Double.parseDouble(tfee);
		String osd = request.getParameter("otherStudentDetail"); 
		
		    Student e = new Student();
		e.setId(student_id);
	    e.setRollno(rollno);
	    e.setgender(gen);
	    e.setfirstname(fname);
	    e.setlastname(lname);
	    e.setDOB(dob);
	    e.setTuition_fee(tf);
	    e.setOther_student_detail(osd);

	    String dateFrom = request.getParameter("ClassDateFrom");
		String dateTo = request.getParameter("ClassDateFrom");
		int classes_id = Integer.parseInt(request.getParameter("classes_id"));
		
		String studentAddressFrom = request.getParameter("studentAddressFrom");
		String studentAddressTo = request.getParameter("studentAddressTo");
		String addressDetail = request.getParameter("addressDetail");
		//int address_id = Integer.parseInt(request.getParameter("address_id"));		
		Class ad = ClassDao.getClassById(classes_id);
		
		
		StudentClasses sc = new StudentClasses();
		sc.setDateAddressFrom(dateFrom);
		sc.setDateAddressTo(dateTo);
		sc.setClasses(ad);
		sc.setStudent_id(student_id);
	

		e.setStudentClasses(sc);
		
	//Addresses address = addressesDao.getAddressById(address_id);
			
		StudentAddress addr = new StudentAddress();
		
		addr.setDateAddressFrom(studentAddressFrom);
		addr.setDateAddressTo(studentAddressTo);
		addr.setAddressDetail(addressDetail);
		addr.setStudent_id(student_id);
		e.setStudentAddress(addr);
		
		int status=StudentDao.update(e) ;
		
        if(status>0){  
            response.sendRedirect("ViewServlet");  
        }else{  
            pw.println("Sorry! unable to update record");  
        }
		
	}
		

}

package com.shah.servlet.Student;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.SchoolDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Class;
import com.shah.pojo.School;
import com.shah.pojo.Student;

/**
 * Servlet implementation class EditServlet1
 */
@WebServlet("/EditServlet1")
public class EditStudentServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditStudentServlet1() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 *      protected void doGet(HttpServletRequest request, HttpServletResponse
	 *      response) throws ServletException, IOException { // TODO Auto-generated
	 *      method stub
	 *      response.getWriter().append("Served at: ").append(request.getContextPath());
	 *      }
	 */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);
		
		Student studentObject = (Student) StudentDao.getStudentById(id);
		request.setAttribute("studentObject", studentObject);
		
		ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
		request.setAttribute("schoolList", schoolList);

		ArrayList<Class> ClassList = (ArrayList<Class>) ClassDao.getAllClasses();
		request.setAttribute("ClassList", ClassList);

		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();
		request.setAttribute("studentList", studentList);

		
		RequestDispatcher rd = request.getRequestDispatcher("StudentEdit.jsp");
		rd.include(request, response);

	}

}

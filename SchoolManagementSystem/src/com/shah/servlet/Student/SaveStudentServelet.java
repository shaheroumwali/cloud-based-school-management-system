package com.shah.servlet.Student;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ClassDao;
import com.shah.dao.SchoolDao;
import com.shah.dao.StudentDao;

import com.shah.pojo.Class;
import com.shah.pojo.School;
import com.shah.pojo.Student;
import com.shah.pojo.StudentAddress;
import com.shah.pojo.StudentClasses;
import com.shah.pojo.User;

/**
 * Servlet implementation class SaveServelet
 */
@WebServlet("/SaveStudentServelet")
public class SaveStudentServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveStudentServelet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 * 
	 *      protected void doGet(HttpServletRequest request, HttpServletResponse
	 *      response) throws ServletException, IOException { // TODO Auto-generated
	 *      response.getWriter().append("Served at: ").append(request.getContextPath());
	 *      }
	 */

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		PrintWriter pw = response.getWriter();
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
			
			String rollNo = request.getParameter("rollno");
			if(StudentDao.isRollNoAvailable(rollNo)) {
				

		response.setContentType("text/html");
		//PrintWriter pw = response.getWriter();
		String rn = request.getParameter("rollno");
		int rollno = Integer.parseInt(rn);
		String gen = request.getParameter("gender");
		String fname = request.getParameter("firstname");
		String lname = request.getParameter("lastname");
		String dob = request.getParameter("dateofbirth");
		String tfee = request.getParameter("tuitionfee");
		double tuitionfee = Double.parseDouble(tfee);
		String osd = request.getParameter("otherStudentDetail");
		int school_id = Integer.parseInt(request.getParameter("school_id"));
		School s = SchoolDao.getSchoolById(school_id);

		Student e = new Student();
		e.setRollno(rollno);
		e.setgender(gen);
		e.setfirstname(fname);
		e.setlastname(lname);
		e.setDOB(dob);
		e.setTuition_fee(tuitionfee);
		e.setOther_student_detail(osd);
		e.setSchool(s);
//				e.setCreated_by(u.getId());
//				e.setModified_by(u.getId());
		e.setCreated_by(0);
		e.setModified_by(0);

		String dateFrom = request.getParameter("ClassDateFrom");
		String dateTo = request.getParameter("ClassDateFrom");
		int classes_id = Integer.parseInt(request.getParameter("classes_id"));
		
		String studentAddressFrom = request.getParameter("studentAddressFrom");
		String studentAddressTo = request.getParameter("studentAddressTo");
		String addressDetail = request.getParameter("addressDetail");
		//int address_id = Integer.parseInt(request.getParameter("address_id"));		
		Class ad = ClassDao.getClassById(classes_id);
		
		
		StudentClasses sc = new StudentClasses();
		sc.setDateAddressFrom(dateFrom);
		sc.setDateAddressTo(dateTo);
		sc.setClasses(ad);
	

		e.setStudentClasses(sc);
		
	//Addresses address = addressesDao.getAddressById(address_id);
			
		StudentAddress addr = new StudentAddress();
		
		addr.setDateAddressFrom(studentAddressFrom);
		addr.setDateAddressTo(studentAddressTo);
		addr.setAddressDetail(addressDetail);
		e.setStudentAddress(addr);
		

		int status = StudentDao.save(e);

		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");	
			request.getRequestDispatcher("EntryStudentServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		}else {
			pw.println("RollNo is already in database...");
		}
			}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}
	}
}

package com.shah.servlet.Student;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.SchoolDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.School;
import com.shah.pojo.Student;
import com.shah.pojo.User;

/**
 * Servlet implementation class ViewServlet
 */
@WebServlet("/ViewServlet")
public class ViewStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewStudentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { // TODO Auto-generated
	 * method stub response.getWriter().append("Served at:
	 * ").append(request.getContextPath()); }
	 * 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

			User u = (User) session.getAttribute("CUSER");
			if (u.getId() >= 1) {

				School school = SchoolDao.getSchoolByUserId(u.getId());
				
				List<Student> list = StudentDao.getAllStudentBySchoolId(school.getId());
				request.setAttribute("StudentList", list);
				RequestDispatcher rd = request.getRequestDispatcher("StudentView.jsp");
				rd.include(request, response);

			}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
		}

	}

}

package com.shah.servlet.userSchoolAndClasses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolDao;
import com.shah.dao.UserDao;
import com.shah.pojo.School;
import com.shah.pojo.User;

/**
 * Servlet implementation class AddUserSchoolServlet
 */
@WebServlet("/AddUserSchoolServlet")
public class AddUserSchoolServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddUserSchoolServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ArrayList<User> userList = (ArrayList<User>) UserDao.getAllUsers();
		request.setAttribute("userList", userList);

		ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
		request.setAttribute("schoolList", schoolList);
		
		RequestDispatcher rd = request.getRequestDispatcher("UserSchool.jsp");
		rd.include(request, response);

	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

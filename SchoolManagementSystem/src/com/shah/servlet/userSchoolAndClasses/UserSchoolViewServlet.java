package com.shah.servlet.userSchoolAndClasses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserSchoolDao;
import com.shah.pojo.UserSchool;

/**
 * Servlet implementation class UserSchoolViewServlet
 */
@WebServlet("/UserSchoolViewServlet")
public class UserSchoolViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSchoolViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		ArrayList<UserSchool> userSchoolList =(ArrayList<UserSchool>) UserSchoolDao.getAllUserSchoolWithSchoolNameandAddrs();
		request.setAttribute("userSchoolList", userSchoolList);
		
		RequestDispatcher rd = request.getRequestDispatcher("UserSchool_View.jsp");
		rd.include(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

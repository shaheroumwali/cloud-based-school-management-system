package com.shah.servlet.userSchoolAndClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.SchoolDao;
import com.shah.dao.UserDao;
import com.shah.dao.UserSchoolDao;
import com.shah.pojo.School;
import com.shah.pojo.User;
import com.shah.pojo.UserSchool;

/**
 * Servlet implementation class UserSchoolSaveServlet
 */
@WebServlet("/UserSchoolSaveServlet")
public class UserSchoolSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSchoolSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();

	if (session.getAttribute("CUSER") != null) {

	User us = (User) session.getAttribute("CUSER");
	if (us.getId() >= 1) {
		int user_id = Integer.parseInt(request.getParameter("user_id"));
		int school_id = Integer.parseInt(request.getParameter("school_id"));

		School p = SchoolDao.getSchoolById(school_id);
		User u = UserDao.getUserById(user_id);
		
		UserSchool e = new UserSchool();
		e.setUser(u);		
		e.setSchool(p);
		
		int status = UserSchoolDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");

			request.getRequestDispatcher("AddUserSchoolServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}


	}
	} else {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("Not authorized...");
}

}
}
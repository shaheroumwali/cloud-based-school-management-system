package com.shah.servlet.userSchoolAndClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.UserSchoolDao;
import com.shah.pojo.UserSchool;

/**
 * Servlet implementation class UserSchoolEditSaveServlet
 */
@WebServlet("/UserSchoolEditSaveServlet")
public class UserSchoolEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSchoolEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter pw = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		int user_id = Integer.parseInt(request.getParameter("user_id"));
		int school_id = Integer.parseInt(request.getParameter("school_id"));

		
		UserSchool e = new UserSchool();
		e.setId(id);
		e.setUser_id(user_id);		
		e.setSchool_id(school_id);
		
		int status = UserSchoolDao.update(e);
		if (status > 0) {
			  response.sendRedirect("UserSchoolViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}
	}
	}



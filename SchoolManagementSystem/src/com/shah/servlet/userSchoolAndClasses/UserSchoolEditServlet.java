package com.shah.servlet.userSchoolAndClasses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.SchoolDao;
import com.shah.dao.UserDao;
import com.shah.dao.UserSchoolDao;
import com.shah.pojo.School;
import com.shah.pojo.User;
import com.shah.pojo.UserSchool;

/**
 * Servlet implementation class UserSchoolEditServlet
 */
@WebServlet("/UserSchoolEditServlet")
public class UserSchoolEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSchoolEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);    
	    
		
		UserSchool userSchoolList =(UserSchool) UserSchoolDao.getUserSchoolById(id);
		request.setAttribute("userSchoolList", userSchoolList);
	    
	    ArrayList<User> userList = (ArrayList<User>) UserDao.getAllUsers();
		request.setAttribute("userList", userList);
		
	    ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
	    request.setAttribute("schoolList", schoolList);	
	    
	    
	    RequestDispatcher rd =request.getRequestDispatcher("UserSchool_Edit.jsp");
	    rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	*/
}

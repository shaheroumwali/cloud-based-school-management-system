package com.shah.servlet.payementType;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.PayementTypeDao;
import com.shah.pojo.PayementType;

/**
 * Servlet implementation class PayementTypeEditServlet1
 */
@WebServlet("/PayementTypeEditServlet1")
public class PayementTypeEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayementTypeEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid); 
	    
		String name = request.getParameter("name");
		PayementType e = new PayementType();
		e.setId(id);
		e.setName(name);
		int status = PayementTypeDao.update(e);
		  if(status>0){  
	        	request.setAttribute("successMsg", "Update Record saved successfully!");
	            response.sendRedirect("PayementTypeViewServlet");  
	        }else{  
	        	request.setAttribute("errorMsg", "Sorry! unable to save record");
	        }

	}

}

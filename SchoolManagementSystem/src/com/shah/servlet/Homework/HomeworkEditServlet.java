package com.shah.servlet.Homework;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.HomeworkDao;
import com.shah.pojo.Homework;
/**
 * Servlet implementation class HomeworkEditServlet
 */
@WebServlet("/HomeworkEditServlet")
public class HomeworkEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeworkEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid); 
		String dateCreated = request.getParameter("DC");
		String Homework = request.getParameter("HM");
		String G = request.getParameter("grade"); 
		int grade = Integer.parseInt(G);
		String otherhomework = request.getParameter("OHD");
		int  student_id = Integer.parseInt(request.getParameter("student_id"));
		
		            Homework e =new Homework();
		            
		e.setId(id);
		e.setDateCreated(dateCreated);
		e.setHomeworkContent(Homework);
		e.setGrade(grade);
		e.setOtherhomeworkDetail(otherhomework);
		e.setStudents_id(student_id);
		int status=HomeworkDao.update(e);
		
        if(status>0){  
            response.sendRedirect("HomeworkViewServlet");  
        }else{  
            out.println("Sorry! unable to update record");  
        }

	}

}

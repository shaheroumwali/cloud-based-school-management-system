package com.shah.servlet.Homework;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.HomeworkDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Homework;
import com.shah.pojo.Student;
import com.shah.pojo.User;


/**
 * Servlet implementation class HomeworkSaveServlet
 */
@WebServlet("/HomeworkSaveServlet")
public class HomeworkSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeworkSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		
		
		
		HttpSession session = request.getSession();
		if(session.getAttribute("CUSER") != null) {
			User u =(User) request.getAttribute("CUSER");
			if(u.getId()>=1) {
			
		PrintWriter out = response.getWriter();
		String dateCreated = request.getParameter("DC");
		String Homework = request.getParameter("Homework");
		//String grade = request.getParameter("gdee");
		//int gdee = Integer.parseInt(grade);
		int grade = Integer.parseInt(request.getParameter("gdee"));
	    String otherhomework = request.getParameter("OHD");
		int student_id = Integer.parseInt(request.getParameter("student_id"));
		
		Student p = StudentDao.getStudentById(student_id);
		
		Homework e =new Homework();
		e.setDateCreated(dateCreated);
		e.setHomeworkContent(Homework);
		e.setGrade(grade);
		e.setOtherhomeworkDetail(otherhomework);
		e.setStudent(p);
		
		int status = HomeworkDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddHomeworkServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		out .close();

	}
		}
		else {
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
			}
	}
		}


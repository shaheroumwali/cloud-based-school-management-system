package com.shah.servlet.schoolAdresses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolAddressDao;

import com.shah.pojo.SchoolAddress;

/**
 * Servlet implementation class SchoolAddrEditServlet
 */
@WebServlet("/SchoolAddrEditSaveServlet")
public class SchoolAddrEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SchoolAddrEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
	    
		int address_id = Integer.parseInt(request.getParameter("address_id"));
		int school_id = Integer.parseInt(request.getParameter("school_id"));

		SchoolAddress e = new SchoolAddress();
		e.setId(id);
		e.setSchool_id(school_id);
		e.setAddresses_id(address_id);
		
		int status = SchoolAddressDao.update(e);
		
		if (status > 0) {
			  response.sendRedirect("SchoolAdrressViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}

		
	}

}

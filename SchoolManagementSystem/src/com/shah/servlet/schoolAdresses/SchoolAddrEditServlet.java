package com.shah.servlet.schoolAdresses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.SchoolAddressDao;
import com.shah.dao.SchoolDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.School;
import com.shah.pojo.SchoolAddress;

/**
 * Servlet implementation class SchoolAddrEditDeleteServlet
 */
@WebServlet("/SchoolAddrEditServlet")
public class SchoolAddrEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SchoolAddrEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);    
	    
	    SchoolAddress schoolAddressList = SchoolAddressDao.getSchoolAddressById(id); 
	    request.setAttribute("schoolAddressList", schoolAddressList);
	    
	    
	    ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
	    request.setAttribute("schoolList", schoolList);	
	    
	    ArrayList<Addresses> addressesList=(ArrayList<Addresses>) addressesDao.getAllAddresses();  
	    request.setAttribute("addressList", addressesList);
	    
	    RequestDispatcher rd =request.getRequestDispatcher("SchoolAddr_Edit.jsp");
	    rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

package com.shah.servlet.schoolAdresses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.School;

/**
 * Servlet implementation class AddSchoolAddressSaveServlet
 */
@WebServlet("/AddSchoolAddressSaveServlet")
public class AddSchoolAddressSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddSchoolAddressSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
		request.setAttribute("schoolList" , schoolList);
		
		ArrayList<Addresses> addressList = (ArrayList<Addresses>) addressesDao.getAllAddresses();
		request.setAttribute("addressList", addressList);
		
		

		RequestDispatcher rd = request.getRequestDispatcher("SchoolAddress.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

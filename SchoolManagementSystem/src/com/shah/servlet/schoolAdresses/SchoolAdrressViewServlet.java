package com.shah.servlet.schoolAdresses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolAddressDao;
import com.shah.pojo.SchoolAddress;

/**
 * Servlet implementation class SchoolAdrressViewServlet
 */
@WebServlet("/SchoolAdrressViewServlet")
public class SchoolAdrressViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SchoolAdrressViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<SchoolAddress> schoolAddressList =(ArrayList<SchoolAddress>) SchoolAddressDao.getAllSchoolAddressWithSchoolNameandAddrs();
		request.setAttribute("schoolAddressList", schoolAddressList);
		
		RequestDispatcher rd = request.getRequestDispatcher("SchoolAddr_View.jsp");
		rd.include(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

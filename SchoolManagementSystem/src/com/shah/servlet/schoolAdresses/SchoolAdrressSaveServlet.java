package com.shah.servlet.schoolAdresses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.SchoolAddressDao;
import com.shah.dao.SchoolDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.School;
import com.shah.pojo.SchoolAddress;
import com.shah.pojo.User;

/**
 * Servlet implementation class SchoolAdrressSaveServlet
 */
@WebServlet("/SchoolAdrressSaveServlet")
public class SchoolAdrressSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SchoolAdrressSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 *      protected void doGet(HttpServletRequest request, HttpServletResponse
	 *      response) throws ServletException, IOException { // TODO Auto-generated
	 *      method stub
	 *      response.getWriter().append("Served at: ").append(request.getContextPath());
	 *      }
	 */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
			
		}
		int address_id = Integer.parseInt(request.getParameter("address_id"));
		int school_id = Integer.parseInt(request.getParameter("school_id"));

		School p = SchoolDao.getSchoolById(school_id);

		Addresses ad = addressesDao.getAddressById(address_id);

		SchoolAddress e = new SchoolAddress();
		e.setSchool(p);
		e.setAddresses(ad);

		int status = SchoolAddressDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");

			request.getRequestDispatcher("AddSchoolAddressSaveServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

	}

}

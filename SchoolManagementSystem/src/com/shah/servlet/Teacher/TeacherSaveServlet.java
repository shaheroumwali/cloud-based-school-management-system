package com.shah.servlet.Teacher;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.SchoolDao;
import com.shah.dao.TeacherDao;
import com.shah.pojo.School;
import com.shah.pojo.Teacher;
import com.shah.pojo.User;
/**
 * Servlet implementation class TeacherSaveServlet
 */
@WebServlet("/TeacherSaveServlet")
public class TeacherSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}  
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
	 	    String fname = request.getParameter("firstname");
			String  lname = request.getParameter("lastname");
			String gen = request.getParameter("gender");
			String OTD = request.getParameter("otherTDetail");
			int school_id = Integer.parseInt(request.getParameter("school_id"));
			
			School s =SchoolDao.getSchoolById(school_id);
			
			    Teacher e = new Teacher();
			    e.setFirstname(fname);
			    e.setLastname(lname);
			    e.setGender(gen);
			    e.setOTD(OTD);
			    e.setSchool(s);
		int status=TeacherDao.save(e);
		
        if(status>0){  
        	request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddTeacherServlet").include(request, response);
        }else{  
			request.setAttribute("errorMsg", "Sorry! unable to save record");
        }

	}
		
	} else {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("Not authorized...");
}

	}
	
	}

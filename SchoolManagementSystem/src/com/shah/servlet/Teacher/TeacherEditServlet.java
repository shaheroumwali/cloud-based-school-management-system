package com.shah.servlet.Teacher;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.TeacherDao;
import com.shah.pojo.Teacher;

/**
 * Servlet implementation class TeacherEditServlet
 */
@WebServlet("/TeacherEditServlet")
public class TeacherEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
	 	PrintWriter pw = response.getWriter();
	 	 String sid=request.getParameter("id");  
		    int id=Integer.parseInt(sid); 
	 	    String fname = request.getParameter("firstname");
			String  lname = request.getParameter("lastname");
			String gen = request.getParameter("gender");
			String OTD = request.getParameter("otherTDetail");
			int school_id = Integer.parseInt(request.getParameter("school_id"));
			    Teacher e = new Teacher();
			    e.setId(id);
			    e.setFirstname(fname);
			    e.setLastname(lname);
			    e.setGender(gen);
			    e.setOTD(OTD);
			    e.setSchools_id(school_id);
		int status=TeacherDao.update(e);
		
        if(status>0){  
            response.sendRedirect("TeacherViewServlet");  
        }else{  
            pw.println("Sorry! unable to update record");  
        }

	}

}

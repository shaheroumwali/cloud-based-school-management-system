package com.shah.servlet.Teacher;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SchoolDao;
import com.shah.dao.TeacherDao;
import com.shah.pojo.School;
import com.shah.pojo.Teacher;


/**
 * Servlet implementation class TeacherEditServlet1
 */
@WebServlet("/TeacherEditServlet1")
public class TeacherEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);  
	      
	    Teacher teacherList = TeacherDao.getTeacherById(id);
	    request.setAttribute("teacherList", teacherList);
	    
	    ArrayList<School> schoolList = (ArrayList<School>) SchoolDao.getAllSchools();
	    request.setAttribute("schoolList", schoolList);
	    
	    RequestDispatcher rd = request.getRequestDispatcher("Teacher_Edit.jsp");
	    rd.include(request, response);
	    
	    
	  
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	*/
	

}

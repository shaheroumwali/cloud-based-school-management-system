package com.shah.servlet.addresses;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.addressesDao;

/**
 * Servlet implementation class AdressDeleteServlet
 */
@WebServlet("/AdressDeleteServlet")
public class AdressDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdressDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
*/	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String sid =request.getParameter("id");
		int id=Integer.parseInt(sid);
		addressesDao.delete(id);
		response.sendRedirect("AddressViewServlet");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("here");
		String sid =request.getParameter("id");
		System.out.println("here");
		int id=Integer.parseInt(sid);
		addressesDao.delete(id);
		System.out.println("here");
		response.sendRedirect("AddressViewServlet");
		System.out.println("here");
	}
*/
}

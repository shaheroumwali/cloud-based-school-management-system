package com.shah.servlet.addresses;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;


/**
 * Servlet implementation class AddressEditServlet
 */
@WebServlet("/AddressEditServlet")
public class AddressEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddressEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
	    String sid=request.getParameter("id");  
	       int id=Integer.parseInt(sid);
	    
	      Addresses list=addressesDao.getAddressById(id);   
	      request.setAttribute("addressList", list);
	      RequestDispatcher rd = request.getRequestDispatcher("address_Edit.jsp");
	      rd.include(request, response);
	      
	    	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

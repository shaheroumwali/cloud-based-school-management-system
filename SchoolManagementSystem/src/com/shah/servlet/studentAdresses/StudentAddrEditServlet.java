package com.shah.servlet.studentAdresses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.StudentAddressDao;
import com.shah.dao.StudentDao;
import com.shah.dao.addressesDao;
import com.shah.pojo.Addresses;
import com.shah.pojo.Student;
import com.shah.pojo.StudentAddress;

/**
 * Servlet implementation class StudentAddrEditServlet
 */
@WebServlet("/StudentAddrEditServlet")
public class StudentAddrEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StudentAddrEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);

		StudentAddress studentAddressList = StudentAddressDao.getStudentAddrById(id);
		request.setAttribute("studentAddressList", studentAddressList);

		ArrayList<Addresses> addressesList = (ArrayList<Addresses>) addressesDao.getAllAddresses();
		request.setAttribute("addressList", addressesList);

		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();
		request.setAttribute("studentList", studentList);

		RequestDispatcher rd = request.getRequestDispatcher("StudentAddr_Edit.jsp");
		rd.include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	       protected void doPost(HttpServletRequest request, HttpServletResponse
	       response) throws ServletException, IOException { 
	    	   // TODO Auto-generated  method stub doGet(request, response); 
	    	   }
	 
}

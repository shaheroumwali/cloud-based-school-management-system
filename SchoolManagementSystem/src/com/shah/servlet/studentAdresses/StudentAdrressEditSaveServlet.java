package com.shah.servlet.studentAdresses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.StudentAddressDao;
import com.shah.pojo.StudentAddress;

/**
 * Servlet implementation class StudentAdrressEditSaveServlet
 */
@WebServlet("/StudentAdrressEditSaveServlet")
public class StudentAdrressEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentAdrressEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
		
		  String dateFrom = request.getParameter("DAF");
			String dateTo  = request.getParameter("DAT");
			String addressDetail = request.getParameter("addressDetail");
			int address_id = Integer.parseInt(request.getParameter("address_id"));
			int student_id = Integer.parseInt(request.getParameter("student_id"));
			
			
			
				    StudentAddress e = new StudentAddress();
				    e.setId(id);
			    e.setDateAddressFrom(dateFrom);
			    e.setDateAddressTo(dateTo);
			    e.setAddressDetail(addressDetail);	
			    e.setAddresses_id(address_id);
			    e.setStudent_id(student_id);

			    
		           int status=StudentAddressDao.update(e);
		           if (status > 0) {
		 			  response.sendRedirect("StudentAddressViewServlet");
		 		} else {
		 			pw.println("Sorry! unable to update record");

		 }
		
	}

}

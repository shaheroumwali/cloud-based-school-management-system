package com.shah.servlet.Parent;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ParentDao;
import com.shah.pojo.Parent;

/**
 * Servlet implementation class ParentEditServlet1
 */
@WebServlet("/ParentEditServlet1")
public class ParentEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ParentEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);  
	      
	    Parent list =ParentDao.getParentById(id);
	    request.setAttribute("parentList", list);
	    
	    RequestDispatcher rd = request.getRequestDispatcher("Parent_Edit.jsp");
        rd.include(request, response);	    
	  

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

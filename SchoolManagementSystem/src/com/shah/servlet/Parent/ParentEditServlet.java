package com.shah.servlet.Parent;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ParentDao;
import com.shah.pojo.Parent;

/**
 * Servlet implementation class ParentEditServlet
 */
@WebServlet("/ParentEditServlet")
public class ParentEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ParentEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
	 	PrintWriter pw = response.getWriter();
	 	    String sid =request.getParameter("id");
		    int id=Integer.parseInt(sid);
	 	    String fname = request.getParameter("firstname");
			String  lname = request.getParameter("lastname");
			String gen = request.getParameter("gender");
			String OTD = request.getParameter("otherPDetail");
			    Parent e = new Parent();
			    e.setId(id);
			    e.setFirstname(fname);
			    e.setLastname(lname);
			    e.setGender(gen);
			    e.setOPD(OTD);
		int status=ParentDao.update(e);
		
        if(status>0){  
            response.sendRedirect("ParentViewServlet");  
        }else{  
            pw.println("Sorry! unable to update record");  
        }


	}

}

package com.shah.servlet.Parent;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ParentDao;
import com.shah.pojo.Parent;
import com.shah.pojo.User;

/**
 * Servlet implementation class ParentSaveServlet
 */
@WebServlet("/ParentSaveServlet")
public class ParentSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ParentSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { // TODO Auto-generated
	 * method stub response.getWriter().append("Served at:
	 * ").append(request.getContextPath()); }
	 * 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		response.setContentType("text/html");
	
		String fname = request.getParameter("firstname");
		String lname = request.getParameter("lastname");
		String gen = request.getParameter("gender");
		String OPD = request.getParameter("otherPDetail");
		Parent e = new Parent();
		e.setFirstname(fname);
		e.setLastname(lname);
		e.setGender(gen);
		e.setOPD(OPD);
		int status = ParentDao.save(e);

		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("Parent.jsp").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}
		}
	} else {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("Not authorized...");

		}
	}
	}

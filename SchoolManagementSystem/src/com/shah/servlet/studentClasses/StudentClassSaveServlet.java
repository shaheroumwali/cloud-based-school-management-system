package com.shah.servlet.studentClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ClassDao;
import com.shah.dao.StudentClassDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Class;
import com.shah.pojo.Student;
import com.shah.pojo.StudentClasses;
import com.shah.pojo.User;

/**
 * Servlet implementation class StudentClassSaveServlet
 */
@WebServlet("/StudentClassSaveServlet")
public class StudentClassSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentClassSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
		response.setContentType("text/html");
	 	    String dateFrom = request.getParameter("DAF");
			String dateTo  = request.getParameter("DAT");
			int classes_id = Integer.parseInt(request.getParameter("classes_id"));
			int student_id = Integer.parseInt(request.getParameter("student_id"));
			
			
			Class ad = ClassDao.getClassById(classes_id);
			Student s = StudentDao.getStudentById(student_id);
				    StudentClasses e = new StudentClasses();
			    e.setDateAddressFrom(dateFrom);
			    e.setDateAddressTo(dateTo);	
			    e.setClasses(ad);
			    e.setStudent(s);

			    
		           int status=StudentClassDao.save(e);
        if(status>0){ 
        	request.setAttribute("successMsg", "Record saved successfully!"); 
        	request.getRequestDispatcher("AddStudentClassServlet").include(request, response);  
        }else{  
        	request.setAttribute("errorMsg", "Sorry! unable to save record"); 
        }


	}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}


}
}
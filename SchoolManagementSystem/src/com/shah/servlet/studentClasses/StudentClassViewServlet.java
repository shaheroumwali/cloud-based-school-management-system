package com.shah.servlet.studentClasses;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.StudentClassDao;
import com.shah.pojo.StudentClasses;

/**
 * Servlet implementation class StudentClassViewServlet
 */
@WebServlet("/StudentClassViewServlet")
public class StudentClassViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentClassViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		List<StudentClasses> studentClasseslist=StudentClassDao.getAllStudentClassWithClassandStudentNames();  
		request.setAttribute("studentClassesList", studentClasseslist);
		
		RequestDispatcher rd = request.getRequestDispatcher("StudentClass_View.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

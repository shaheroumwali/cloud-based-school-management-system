package com.shah.servlet.studentClasses;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shah.dao.StudentClassDao;
import com.shah.pojo.StudentClasses;

/**
 * Servlet implementation class StudentClassEditSaveServlet
 */
@WebServlet("/StudentClassEditSaveServlet")
public class StudentClassEditSaveServlet extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StudentClassEditSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException {
	 * 
	 * }
	 * 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter pw = response.getWriter();
		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);

		String dateFrom = request.getParameter("DAF");
		String dateTo = request.getParameter("DAT");
		int classes_id = Integer.parseInt(request.getParameter("classes_id"));
		int student_id = Integer.parseInt(request.getParameter("student_id"));

		StudentClasses e = new StudentClasses();
		e.setId(id);
		e.setDateAddressFrom(dateFrom);
		e.setDateAddressTo(dateTo);
		e.setClass_id(classes_id);
		e.setStudent_id(student_id);

		int status = StudentClassDao.update(e);

		if (status > 0) {
			response.sendRedirect("StudentClassViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

		}
	}

}

package com.shah.servlet.studentClasses;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.StudentClassDao;
import com.shah.dao.StudentDao;
import com.shah.pojo.Class;
import com.shah.pojo.Student;
import com.shah.pojo.StudentClasses;

/**
 * Servlet implementation class StudentClassEditServlet
 */
@WebServlet("/StudentClassEditServlet")
public class StudentClassEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentClassEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);

		StudentClasses studentClassesList = StudentClassDao.getStudentClasById(id);
		request.setAttribute("studentClassesList", studentClassesList);

		ArrayList<Class> classList = (ArrayList<Class>) ClassDao.getAllClasses();
		request.setAttribute("classList", classList);

		ArrayList<Student> studentList = (ArrayList<Student>) StudentDao.getAllStudents();
		request.setAttribute("studentList", studentList);
		
		RequestDispatcher rd  = request.getRequestDispatcher("StudentClass_Edit.jsp");
		rd.include(request, response);
     
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}

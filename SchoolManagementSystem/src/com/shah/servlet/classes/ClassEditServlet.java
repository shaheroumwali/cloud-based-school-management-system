package com.shah.servlet.classes;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.SubjectDao;
import com.shah.dao.TeacherDao;
import com.shah.pojo.Class;
import com.shah.pojo.Subjects;
import com.shah.pojo.Teacher;

/**
 * Servlet implementation class ClassEditServlet
 */
@WebServlet("/ClassEditServlet")
public class ClassEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClassEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));

		Class classlist = ClassDao.getClassById(id);
		request.setAttribute("classlist", classlist);

	ArrayList<Teacher> teacherList =(ArrayList<Teacher>) TeacherDao.getAllTeachers();
		request.setAttribute("teacherList", teacherList);

		ArrayList<Subjects> subjectList =(ArrayList<Subjects>) SubjectDao.getAllSubjects();
		request.setAttribute("subjectList", subjectList);

		RequestDispatcher rd = request.getRequestDispatcher("Class_Edit.jsp");
		rd.include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

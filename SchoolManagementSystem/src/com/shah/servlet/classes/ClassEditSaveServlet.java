package com.shah.servlet.classes;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shah.dao.ClassDao;
import com.shah.pojo.Class;
import com.shah.pojo.User;


/**
 * Servlet implementation class ClassEditSaveServlet
 */
@WebServlet("/ClassEditSaveServlet")
public class ClassEditSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClassEditSaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
response.setContentType("text/html");
		
		HttpSession session = request.getSession();

		if (session.getAttribute("CUSER") != null) {

		User u = (User) session.getAttribute("CUSER");
		if (u.getId() >= 1) {
			
		}
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");   
	    int id=Integer.parseInt(sid); 
		String cod = request.getParameter("code");
		String name = request.getParameter("name");
		String dateFrom = request.getParameter("DAFrom");
		String dateTo = request.getParameter("DAT");
		int teacher_id = Integer.parseInt(request.getParameter("teacher_id"));
		int subject_id = Integer.parseInt(request.getParameter("subject_id"));
		
		
		com.shah.pojo.Class e = new Class();
		e.setId(id);
		e.setCode(cod);
		e.setName(name);
		e.setDateAddressFrom(dateFrom);
		e.setDateAddressTo(dateTo);
		e.setTeacher_id(teacher_id);
		e.setSubject_id(subject_id);

		int status = ClassDao.update(e);
		if (status > 0) {
			  response.sendRedirect("ClassViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}
		} else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Not authorized...");
	}

	}
	}



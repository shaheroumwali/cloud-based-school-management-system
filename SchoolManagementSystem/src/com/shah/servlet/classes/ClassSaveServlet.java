package com.shah.servlet.classes;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.ClassDao;
import com.shah.dao.SubjectDao;
import com.shah.dao.TeacherDao;
import com.shah.pojo.Class;
import com.shah.pojo.Subjects;
import com.shah.pojo.Teacher;

/**
 * Servlet implementation class ClassSaveServlet
 */
@WebServlet("/ClassSaveServlet")
public class ClassSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClassSaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		String cod = request.getParameter("code");
		String name = request.getParameter("name");
		String dateFrom = request.getParameter("DAF");
		String dateTo = request.getParameter("DAT");
		int teacher_id = Integer.parseInt(request.getParameter("teacher_id"));
		int subject_id = Integer.parseInt(request.getParameter("subject_id"));

		Teacher p = TeacherDao.getTeacherById(teacher_id);
		Subjects s = SubjectDao.getSubjectypeById(subject_id);

		Class e = new Class();
		e.setCode(cod);
		e.setName(name);
		e.setDateAddressFrom(dateFrom);
		e.setDateAddressTo(dateTo);
		e.setTeacher(p);
		e.setSubject(s);
		

		int status = ClassDao.save(e);
		if (status > 0) {
			request.setAttribute("successMsg", "Record saved successfully!");
			request.getRequestDispatcher("AddClassSaveServlet").include(request, response);
		} else {
			request.setAttribute("errorMsg", "Sorry! unable to save record");
		}

	}

}

package com.shah.servlet.classes;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SubjectDao;
import com.shah.dao.TeacherDao;
import com.shah.pojo.Subjects;
import com.shah.pojo.Teacher;

/**
 * Servlet implementation class AddClassSaveServlet
 */
@WebServlet("/AddClassSaveServlet")
public class AddClassSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddClassSaveServlet() {   
        super();   
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<Teacher> TeacherList = (ArrayList<Teacher>) TeacherDao.getAllTeachers()  ;
		request.setAttribute("TeacherList", TeacherList);
			
		ArrayList<Subjects> SubjectList = (ArrayList<Subjects>) SubjectDao.getAllSubjects();
		request.setAttribute("SubjectList", SubjectList);
	
		RequestDispatcher rd = request.getRequestDispatcher("Classes.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.shah.servlet.subject;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SubjectDao;
import com.shah.pojo.Subjects;

/**
 * Servlet implementation class SubjectEditServlet1
 */
@WebServlet("/SubjectEditServlet1")
public class SubjectEditServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectEditServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /**
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid); 
		String name = request.getParameter("sname");
	         	Subjects e = new Subjects();
		e.setId(id);
		e.setSubjecttName(name);
		int status = SubjectDao.update(e);
		if (status > 0) {
			pw.print("<p> Update Subject successfully!</p>");
			 response.sendRedirect("SubjectViewServlet");
		} else {
			pw.println("Sorry! unable to update record");

}

	}

}

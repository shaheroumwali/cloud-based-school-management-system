package com.shah.servlet.subject;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shah.dao.SubjectDao;
import com.shah.pojo.Subjects;

/**
 * Servlet implementation class SubjectEditServlet
 */
@WebServlet("/SubjectEditServlet")
public class SubjectEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
	    String sid=request.getParameter("id");  
	    int id=Integer.parseInt(sid);       
	    Subjects subjectList=SubjectDao.getSubjectypeById(id);
	    request.setAttribute("subjectList",subjectList);
	    RequestDispatcher rd = request.getRequestDispatcher("Subject._Edit.jsp");
	    rd.include(request, response);
	 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
*/
}
